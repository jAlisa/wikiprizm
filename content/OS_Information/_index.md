---
aliases:
- /Add-in/
- /Add-ins/
- /CASIOWIN/
bookCollapseSection: true
title: CASIOWIN
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

CASIOWIN is the name of the Operating System used on the Prizm and other
Casio calculators. Note that despite the name being the same across
devices, CASIOWIN is actually quite different from system to system,
even though the editions on newer models are usually the result of the
evolution of the previous model edition. In the case of the Prizm, its
CASIOWIN is the result of the port of fx-9860G's edition of the OS to a
SH7305 CPU, [high-resolution color
screen]({{< ref "Technical_Documentation/Display.md" >}}) and big
[flash]({{< ref "flash.md" >}}) (with a new filesystem for the storage
memory).

On the Prizm, the OS is located in flash from address 0x80020000 to
0x80B5FFFF (a bit over 11 MB) - that is, from after the
[bootloader](bootloader) to before flash regions that are used to store
data that varies from device to device, like currently selected add-in
language, user information (that is shown on power off), [Main
Memory]({{< ref "File_System.md" >}}) backups, and of course, the
[Storage Memory]({{< ref "File_System.md" >}}).

## Features unique to the Prizm {#features_unique_to_the_prizm}

Here are some features of CASIOWIN which were introduced with the Prizm,
some of which have not yet found their way to updates for older models:

-   Full-color, high-resolution screen support;
    -   Color picture displaying in proprietary format (g3p);
    -   Colored graphs;
    -   Color-link on Statistics and Spreadsheet;
    -   Brackets coloring on certain expression inputs;
    -   Addition of a color option to lots of things.
-   Status area (current angle unit, display mode, complex mode...);
    -   Battery meter on the status area, along with "battery type"
        setting;
-   [USB]({{< ref "USB_Communication.md" >}}) mass-storage function
    (previously, link software was necessary);
-   "Proper" [flash]({{< ref "flash.md" >}}) file system, with support
    for sub-folders (3 levels on the native OS file browsers,
    theoretically unlimited in custom add-ins);
-   Support for Chinese language (the OS includes Hanzi/kanji, Hiragana
    and Katakana characters);
-   Dual screen (Graph+Graph or Graph to Table);
-   Inclusion of more [fonts]({{< ref "fonts.md" >}})...

## Start-up/shutdown processes {#start_upshutdown_processes}

*Note: this is very incomplete, things may be in the wrong order, etc.
More investigation is needed.*

### Cold-boot start-up {#cold_boot_start_up}

This is what happens when the OS starts up for the first time after
batteries having been removed, after the RESTART button is pushed, or
when a [software reset]({{< ref "Syscalls/Restart.md" >}}) is performed.

After execution jumps to the OS entry point, thanks to the
[bootloader](bootloader), it proceeds to initialize various memory
areas, mounts the storage memory file system using the Fugue API, loads
MCS and [setup]({{< ref "setup.md" >}}) backups from flash, if they
exist (at this point the brightness is set to the user-chosen level) and
shows a set-up wizard where users can choose for example language,
display brightness and battery type. This wizard is shown even if the
users have already set these settings and the OS still remembers them;
it is skipped on the "fx-CG10/20 Manager" emulator. Finally, the Main
Menu is shown.

### User-requested standby {#standby}

After the user presses Shift+AC/On,
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) calls
[PowerOff]({{< ref "Syscalls/PowerOff.md" >}}) with its parameter set
to 1. This way, the Casio logo is shown. After a delay for showing the
logo, the current VRAM, [stack]({{< ref "Processes.md#stack" >}}),
[heap]({{< ref "Processes.md#heap" >}}), [setup]({{< ref "setup.md" >}}) and
[MCS]({{< ref "File_System.md" >}}) contents, are saved to
[flash]({{< ref "flash.md" >}}) and the code for coming from the standby
state is copied to RS memory. The screen is turned off. After this, the
CPU enters R-Standby Mode, where all peripherals are turned off, clocks
are halted except for RCLK and power is cut to most CPU areas except I/O
areas. The contents of the RS memory and some registers are held in
memory.

### User-requested power on after standby {#user_requested_power_on_after_standby}

After the user presses AC/On, an interrupt is generated, the CPU is
woken from R-Standby, and the interrupt handler code that was copied to
RS memory on power off takes care of turning on the screen, displaying
the busy-indicator, and restoring the stack, heap, VRAM and MCS
contents. Possibly [Setup]({{< ref "Setup.md" >}}) is not restored
because it is stored on RS memory which was kept, it only is backed up
on shutdown in case batteries are removed. Finally, the program
execution continues, and the
[PowerOff]({{< ref "Syscalls/PowerOff.md" >}}) syscall returns -
application execution continues. If the responsible for calling PowerOff
was GetKey, to the running application the result is no different than
any other call to GetKey.

### User-requested power off {#user_requested_power_off}

There are no known methods for safely powering off the calculator to the
point where no state on the running application is kept. The recommended
method is to issue a user-requested standby, which saves the important
RAM areas, then take off the batteries. Since RS memory is not kept, the
next boot will be a cold one, and only MCS, setup and possibly other
settings will be restored from flash, but not the stack or heap.

## Add-in software and language {#add_in_software_and_language}

The OS supports add-in software in the form of g3a files and add-in
languages in the form of g3l files. (It also supports BASIC scripts that
are stored in Main Memory, but they are much less powerful than add-in
software.)

The g3a files consist of a header, containing the add-in name, icons,
other values and checksums, followed by the executable binary. More
information can be found on
[G3A_File_Format]({{< ref "G3A_File_Format.md" >}}).

The g3l files consist of a header, containing the language name and
other values, followed by a list of
[multi-byte_strings]({{< ref "multi-byte_strings.md" >}}).

To date, Casio released five official software add-ins (usually simply
mentioned as "add-ins"): Geometry, Picture Plot, Conversion, Physium and
ProbSim. The first three add-ins were released on launch day, with
Conversion being just a placeholder that the OS checks to enable the
conversion function on the built-in apps. Physium was released with OS
1.03 and that is the lowest version it will run on. ProbSim was released
with OS 2.00 but can work on earlier OS versions.

With OS 2.00, Casio also released the Russian language add-in, which
seems to work with problems in earlier OS versions. More information on
add-in versions can be found on
[Versions_and_versioning]({{< ref "Versions_and_versioning.md" >}}).

For a long time (since at least April 2011) the community has been able
to run custom machine code in the form of unofficial add-ins. Since
then, dozens of unofficial add-ins have been developed (possibly over a
hundred), with varying degrees of stability, user experience and OS
integration.

Add-ins use OS functions as [syscalls]({{< ref "syscalls.md" >}}). Most
OS functions, even insignificant ones, appear to be exposed as syscalls,
but apparently certain libraries (such as zlib, included for decoding
and encoding of g3p pictures) are not exposed. Most system settings and
flags can (and should) also be read and set using syscalls, but there
are some omissions (see for instance
[SetGetkeyToMainFunctionReturnFlag]({{< ref "SetGetkeyToMainFunctionReturnFlag.md" >}})).

Syscalls are called by jumping to address 0x80020070 with a syscall
number in r0, and after the syscall is executed, execution jumps back to
the calling function. More details can be found in the article
[Syscalls]({{< ref "Syscalls.md" >}}) and a list of the known syscalls
can be found [here]({{< ref "Syscalls/" >}}).

Shared libraries are not supported by the OS. This means each add-in has
to be statically linked to any necessary libraries, and the only
external code is in the form of OS-provided syscalls.

## Structure in memory {#structure_in_memory}

The OS is stored in [Flash]({{< ref "Flash.md" >}}) as a single
connected binary which contains the machine code to be executed and data
used by it (messages, bitmaps, etc.). Unlike what happens with full
computer operating systems and some other embedded systems, the OS
doesn't use a filesystem to store its assets and built-in applications;
these are stored entangled with the OS code. Some things like the
[syscall]({{< ref "Syscalls.md" >}}) table and language data have a
fixed offset in memory that stays constant between OS versions, but
these are in the minority.

Data stored by the OS such as [Main
Memory]({{< ref "File_System.md" >}}) backups, backups of stacks and
heap for the [User-requested standby](#User-requested_standby) function,
and [owner name / organization
pairs]({{< ref "User_Name_Registry.md" >}}) displayed when going into
standby also have their own fixed, dedicated locations in Flash,
separate from the [Storage Memory
filesystem]({{< ref "File_System.md" >}}). This has its advantages: such
data does not take storage space, operations like the backing up system
state for standby never fail due to lack of storage space, and are
generally faster since they do not have to deal with filesystem overhead
(including potential problems like fragmentation of state files).

In terms of RAM usage, usually fixed allocation is used (i.e. each thing
has its own preset location), and even though the OS has a
[heap]({{< ref "Processes.md#heap" >}}), it is small compared to the size of the
[stack]({{< ref "Processes.md#stack" >}}), and accounts for less than 7% of the
total RAM address space. Many variables and assets change address
depending on the OS build, but are constant for a given build - the
addresses appear to change at the whim of the compiler used. It's
possible that things that haven't changed until now, like the VRAM
address, will change in the future versions (hence the need to use
syscalls like
[GetVRAMAddress]({{< ref "Syscalls/GetVRAMAddress.md" >}})). Some
variables of interest and locations of resources can't be retrieved
using the known syscalls, meaning indirect means of obtaining such
information must be used (see
[SetGetkeyToMainFunctionReturnFlag]({{< ref "SetGetkeyToMainFunctionReturnFlag.md" >}})
for an example).

## Process and task model {#process_and_task_model}

The OS does not present any multitasking or multithreading mechanisms,
other than [Timers]({{< ref "/OS_Information/Timers.md" >}}), which at a stretch can be
considered a very basic multithreading mechanism. A predefined number of
"processes" is supported. For more information, see the article
[Processes]({{< ref "Processes.md" >}}).

## OS versions {#os_versions}

See the article
[Versions_and_versioning]({{< ref "Versions_and_versioning.md" >}}).

## Secret key combinations {#secret_key_combinations}

See the article
[Secret_Key_Combinations]({{< ref "Secret_Key_Combinations.md" >}}).

## Behavior on program error {#behavior_on_program_error}

See the article [Error_handling]({{< ref "Error_handling.md" >}}).

## Locale

See the article [Locale]({{< ref "Locale.md" >}}).

## Bitmaps

A notable part of the flash occupied by the OS is used to store bitmaps,
from icons and screen backgrounds, to font glyphs. See
[Bitmaps]({{< ref "Bitmaps.md" >}}) for more information.

## Checksum

The OS has a checksum at address 0xA0B5FFF8, which is the sum of the
bytes in the address range 0xA0020000 to 0xA0B5FEAF plus the sum of the
bytes in the address range 0xA0B5FEF0 to 0xA0B5FFF7. The checksum is
verified by the bootloader and possibly the OS itself, and in case of a
mismatch special behavior will be triggered, like entering a emergency
OS update mode.
