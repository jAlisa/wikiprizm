---
revisions:
- author: Gbl08ma
  timestamp: '2017-07-04T12:05:38Z'
title: Examination mode
aliases:
  - /Examination_mode/
---

Since [OS version 02.02]({{< ref "Versions_and_versioning.md" >}}), the
[OS]({{< ref "_index.md" >}}) includes an examination mode. In this
mode, meant to be used during an examination or test, certain calculator
features are locked down, and the calculator presents different behavior
to indicate that this special mode is active.

The built-in Link app is used to provide information and certain
examination mode-related functionality. However, the examination mode
can't be entered directly from this app.

## Behavior in examination mode {#behavior_in_examination_mode}

In the examination mode, the
[Frame]({{< ref "Syscalls/FrameColor.md" >}}) on the right, left and
bottom border of the
[screen]({{< ref "Technical_Documentation/Display.md" >}}) becomes
green, and a flashing R in blue background will be displayed on the
status bar. The rate of flashing is reduced after approximately 15
minutes.

The automatic power off period is locked at 60 minutes.

Pressing Alpha then the minus sign key to the left of EXE, (-), causes a
message box containing the time elapsed in examination mode to appear.

### Unavailable features {#unavailable_features}

The following features are unavailable in examination mode:

-   eActivity, Program, E-Con3 and Memory built-in apps
-   [Add-in software and language]({{< ref "_index.md" >}})
-   Vector math commands
-   File transfer
-   [Storage memory]({{< ref "File_System.md" >}}) access
-   [User information]({{< ref "User_Name_Registry.md" >}}) editing
-   OS Update

The [main memory]({{< ref "File_System.md" >}}) is still available, but
with restrictions: when entering the mode, it is backed up then cleared.
When exiting the mode, the backed up memory contents are restored and
the changes made while in the examination mode are lost.

## Entering the mode {#entering_the_mode}

-   Press **\[SHIFT\]** then **\[AC/ON\]** to put the calculator in
    standby;
-   Press **\[cos\]+\[7\]+\[AC/ON\]** at the same time;
-   Press **\[F1\]**;
-   Press **\[F2\]**;
-   Press **\[EXIT\]**.

## Exiting the mode {#exiting_the_mode}

Officially, the examination mode can be exited in three different ways:

-   Connecting the calculator to a computer using a USB cable, then
    selecting Mass Storage mode (**\[F1\]**) on the mode selection
    screen, and on the computer, copy or delete any file on the
    calculator, then terminate the connection;
-   Allowing 12 hours to elapse. This won't work if the calculator is
    restarted or batteries are taken off during this period; if that
    happens, the timer will reset;
-   Connecting the calculator to another one, not in examination mode
    but with examination mode support (OS 02.02 or later), through a
    link cable. To do this, connect the calculator, open the Link app on
    the second calculator, press **\[F3\]** (EXAM), **\[F1\]** (UNLOCK),
    **\[F1\]**; or you could transfer any data from the second
    calculator to the locked one.

## Consequence of various actions in examination mode {#consequence_of_various_actions_in_examination_mode}

Pressing the RESTART button or removing the batteries will not exit the
examination mode, but it will reset the "elapsed time" timer, and clear
the main memory data entered in examination mode.

Putting the calculator into standby with Shift then AC/On will keep the
main memory contents entered in examination mode.

Resetting the main memory while in examination mode has no effect on the
backup that was saved when entering it.

In other words, while in examination mode, the OS inhibits any attempt
to save the current main memory contents to
[flash]({{< ref "flash.md" >}}), and when exiting this mode, a backup is
restored, making any changes or resets to the main memory irrelevant.
Since the storage memory is not available in this mode, it doesn't need
to be erased or blocked, and it's contents will be available again when
the calculator exits examination mode. The flag, or one of the flags,
that indicates the examination mode is on is clearly stored in
non-volatile memory - the flash.

The effect of the various [secret key
combinations]({{< ref "Secret_Key_Combinations.md" >}}) in this mode,
especially bootloader ones, is not well known yet.
