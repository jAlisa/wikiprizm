---
revisions:
- author: Gbl08ma
  timestamp: '2016-05-10T19:22:33Z'
title: Fonts
aliases:
  - /Fonts/
---

The OS includes, at least, four different fonts. For each font, there's
a set of syscalls which work, and can display strings, with it.

## 24 px font {#px_font}

This is a fixed-width (width = 18 px) font used with syscalls such as
[PrintXY]({{< ref "Syscalls/PrintXY.md" >}}) and
[Print_OS]({{< ref "Syscalls/Print_OS.md" >}}). The screen has space for
exactly nine lines of text displayed in this font, with each line
containing space for 21 characters plus six pixels at the end (where a
[scroll bar]({{< ref "Syscalls/UI_elements/Scrollbar.md" >}}) fits).

### Characters supported {#characters_supported}

Most, if not all, [multi-byte]({{< ref "Multi-byte_strings.md" >}})
characters are supported, including the Latin, Greek and Russian
alphabets.

### Example

Text in the System menu, excluding the function key labels at the
bottom, is displayed in this font:

[1](http://s.lowendshare.com/7/1407155019.502.systmenu.png)

## 18 px font {#px_font_1}

This is a variable-width font used with syscalls such as
[PrintMini]({{< ref "Syscalls/PrintMini.md" >}}) and
[GetMiniGlyphPtr]({{< ref "Syscalls/GetMiniGlyphPtr.md" >}}). The screen
has space for exactly 12 lines of text displayed in this font.

### Characters supported {#characters_supported_1}

Most, if not all, [multi-byte
characters]({{< ref "Multi-byte_strings.md" >}}) are supported,
including the Latin, Greek and Russian alphabets.

### Example {#example_1}

Text in the status area and in eActivity line notes is displayed in this
font:

[2](http://s.lowendshare.com/7/1407155401.37.memodetail.png)

## 16 px font {#px_font_2}

This is a variable-width font which appears to only be used to show app
names in the Main Menu, unless the selected language is Chinese (in such
case, the Main Menu app names are printed using the [#18 px
font](#18_px_font)). It is the font used by
[Bdisp_MMPrint]({{< ref "Syscalls/Bdisp_MMPrint.md" >}}) and
[Bdisp_MMPrintRef]({{< ref "Syscalls/Bdisp_MMPrintRef.md" >}}).

### Characters supported {#characters_supported_2}

The Latin alphabet appears to be supported. The Greek and Cyrillic
alphabet are not supported; this explains why the official Russian
language add-in contains the Main Menu names of the apps in English.

Numbers are supported. Certain special symbols are supported, including
!, ?, #, &, \|, @, { and }. Things like arrow keys and most of the other
Casio-specific symbols are not supported.

### Example {#example_2}

Here's the Main Menu when the Portuguese language is selected, showing a
variety of special characters:

[3](http://s.lowendshare.com/7/1407158667.227.mainmenupt.png)

## 10 px font {#px_font_3}

This is a variable-width font used with syscalls such as
[PrintMiniMini]({{< ref "Syscalls/PrintMiniMini.md" >}}). The screen has
space for 21 lines of text displayed in this font. Its space character
is wider than most characters.

### Characters supported {#characters_supported_3}

Most printable ASCII characters (0x20 to 0x7F) are supported. However,
only a very limited subset of [multi-byte
characters]({{< ref "Multi-byte_strings.md" >}}) is supported.

### Example {#example_3}

The numbers on the graph axis are printed in this font:

[4](http://s.lowendshare.com/7/1407158062.699.graph.png)

And here's another example showing text printed in this font (at the
bottom):

[5](http://s.lowendshare.com/7/1407158164.323.minimini.png)
