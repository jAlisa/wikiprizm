---
revisions:
- author: Gbl08ma
  comment: /\* Bootloader integrity problems \*/
  timestamp: '2015-02-15T00:00:16Z'
title: Error handling
aliases:
  - /Error_handling/
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

[CASIOWIN]({{< ref "_index.md" >}}) handles a big variety of errors,
in more or less user-friendly ways, from user-space and fully expected
errors (like, for instance, a syntax error on a math expression) to
errors due to faulty program code.

The OS sets up CPU parameters so that it can handle exceptions, both
expected ones that happen by design (e.g. TLB miss) and others that
would ideally never be triggered, like those due to illegal opcodes or
illegal memory access. The latter ones cause [System
ERRORs](#System_ERROR).

## User-level expected errors {#user_level_expected_errors}

Errors for which the OS is well prepared and are handled gracefully
include syntax errors on math expressions and BASIC programs, user
inputs out of range, calculation results out of range, wrong number of
elements for math or BASIC functions, invalid data types on fields, etc.
These are errors that normally result from mistakes on the user side and
which are absolutely expected.

For those errors, a message containing a line of text in the form
"\<something> ERROR" is shown. This can be "Syntax ERROR",
"Conversion ERROR", "Range ERROR", etc. Some error messages are more
friendly and descriptive, like "Can't solve! / Adjust initial / value or
bounds. / Then try again." or "Complex Number In List". EXIT, QUIT or
AC/On can be used to break out of these message boxes, usually allowing
for solving the situation that caused the error message.

## User-level "less expected" errors {#user_level_less_expected_errors}

There are also a number of situations for which the OS is prepared, but which
are not meant to happen on a normal basis. This includes file access errors,
data corruption errors, invalid file path errors, and some kinds of errors which
very rarely happen during [proccess switching]({{< ref "Processes.md" >}}).
Error messages for these errors include "Data ERROR", "Data is protected" or
"Application ERROR". Like with more usual errors, these messages can be closed
with EXIT, QUIT or AC/On and, generally, normal operation is resumed.

Another example of a handled but not-so-expected problem is when the
user formats the storage memory on the computer during a USB connection.
In this case, "File System ERROR" is shown and certain operations will
not be available until the user initializes the calculator using the
Reset menu on the System app.

Some of these gracefully-handled errors are mentioned in the "Error
Message Table" section of the Appendix of the Prizm's software manual.

## System ERROR {#system_error}

When some problem is not handled by the OS at a higher level, an
exception occurs at the CPU level. These exceptions are most usually
triggered by custom add-ins with bugs, and, in normal situations, are
handled by the OS. The OS handles them by stopping execution of the
current [process]({{< ref "Processes.md" >}}) to some extent (but not
completely: user [timers]({{< ref "/OS_Information/Timers.md" >}}) will still run, for
example), showing a pop-up on screen with title "System ERROR" and some
information about the fault:

[1](http://s.lowendshare.com/11/1417561924.698.syserrR.png)

From this pop-up, one can press the EXIT key to reboot. Usually this
works, but it's possible that the system got into such a messed state,
that it will not work, and one must reboot the calculator by other
means. The EXE key can be used to continue execution on certain kinds of
exceptions, such as address
exceptions<sup>[Confirmed](User:Ahelper)</sup>, but this is generally
not advised, because it will only make the problem worse.

In the case of system errors during execution of the *child*
[process]({{< ref "Processes.md" >}}), it is sometimes possible to press
the MENU key and get into the Main Menu, open another add-in or built-in
app (therefore ending the current child process, and switching to a new
one), and the calculator will keep working as usual without the need for
rebooting. However, this is not recommended and, depending on the cause
of the error, may not work, causing further system errors or other kind
of undefined behavior and, possibly, data loss. One very real
possibility, is that file handles from the previous child process will
not be closed when leaving a misbehaving app this way, and file
operations may have strange behaviors, especially for files touched by
the misbehaving program.

The pop-up appears to be a normal message box, probably opened with
[MsgBoxPush]({{< ref "Syscalls/UI_elements/MsgBoxPush.md" >}}). In most
cases, it's possible to see the last screen contents from the
misbehaving application behind the message box. The first three lines
are always the same. The fourth line tells the type of exception and may
be "ADDRESS(R)", "ADDRESS(W)", "PROTECTED(R)", "PROTECTED(W)",
"Interrupt", "Illegal Code Err" or "TLB ERROR". More information about
each error can be found in the [Exception Types
section](#Exception_Types). The fifth and sixth lines don't always
appear; when they do, they provide information about the state of
program execution at the time of the exception.

### Exception Types {#exception_types}

The fourth line on the System Error dialog tells you what exception the
OS handled. This gives you an idea of what is wrong with the code at the
PC. The Transition address is the address of the exception handler
called. VBR is the address stored at 0xA0000000.

#### ADDRESS(x)

**Causes:** This exception is thrown if there is a data address error.
This can be caused if a word is accessed across word boundary, a
longword across a longword boundary, or a quadword across a quadword
boundary. It is also possible that this may be called when an
instruction address is used that is not word-aligned. The (x) will
either be *R* or *W* if the exception occurred on the *R*ead or the
*W*rite cycle.

**VBR Offset:** 0x100

**Exception Code:** 0x0E0 on read, 0x100 on write

**Priority:** 5

**Skippable:** Yes

#### PROTECT(x)

**Causes:** This is unconfirmed, however this may be caused by a read or
write to protected memory. The (x) will either be *R* or *W* if the
exception occurred on the *R*ead or the *W*rite cycle.

**VBR Offset:** 0x100

**Exception Code:** 0x0A0 on read, 0x0C0 on write

**Priority:** 7

**Skippable:** Unknown

#### INTERRUPT

**Causes:** This is caused by either a TRAPA #imm (unconditional trap)
assembly opcode or from an illegal slot instruction exception (See SH4a
assembly manual for details).

**VBR Offset:** 0x100

**Exception Code:** 0x160

**Priority:** 4

**Skippable:** Unknown

An example of encountering this exception is by going to the diagnostic
mode, pressing 3, 9, 2, F1. The "SYSTEM ERROR" message appears on screen
with "INTERRUPT", but the target and PC lines are not shown. EXIT can't
be used to reboot, despite what the text
says.<sup>[credits](User:Gbl08ma)</sup>

#### Illegal Code Err {#illegal_code_err}

**Causes:** Assumed to be caused by either an unknown instruction being
called or an opcode generated a general error (such as invalid usage of
branch opcodes generating exceptions).

**VBR Offset:** 0x100

**Exception Code:** 0x180

**Priority:** 4

**Skippable:** Unknown

#### TLB ERROR {#tlb_error}

**Causes:** There was a memory access to the virtual memory that isn't
mapped to a physical memory page in the UTLB page table (and possibly
the ITLB page table if the access was from an instruction fetch) and the
OS was unable to handle it. *Note that the actual exception is normal
behavior for the MMU and relies on the OS to handle it without
generating errors. This occurs if the OS cannot handle it.*

**VBR Offset:** 0x400

**Exception Code:** 0x040 if this was from a TLB miss in the UTLB cache
from a read (Also a miss in the ITLB if this is an instruction fetch),
0x060 if this was from a TLB miss in the UTLB page table from a memory
write.

**Priority:** 2 if from an ITLB miss, 6 if from a UTLB miss.

**Skippable:** Unknown

## Bootloader integrity problems {#bootloader_integrity_problems}

The OS checks the bootloader integrity, or at least its presence, every time the
calculator is [turned on]({{< ref "_index.md#start_upshutdown_processes" >}}).
If the bootloader is damaged, for example, by erasing the first flash sector,
and no other damage is done, the calculator will keep operating until the OS
attempts to reboot or batteries are taken off (or become empty).  During the
period the Prizm and its OS still work, there are no changes to normal operation
except the following:

-   Every time the calculator is turned on, the diagnostic mode will open with
    the message "ABS Mark NG" on the top left, in the place of "LY755D MAIN":

[2](http://s.lowendshare.com/7/1406915278.539.abs1.jpg)

"ABS Mark NG" is shown whenever the diagnostic mode is entered and the 8
bytes starting at 0xA0000338 do not match "CASIOABS". If the only the
"CASIOABS" string is damaged, the calculator will enter a emergency OS
updating mode on the next reboot. This happens even in the "fx-CG10/20
Manager PLUS" emulator.

Performing a ROM checksum test from the diagnostic mode results in this:

[3](http://s.lowendshare.com/7/1406915361.394.abs2.jpg)

It is possible to close the diagnostic menu by pressing EXIT (unlike
what happens when opened normally, it will not restart) and continue
operating the calculator, which may be useful during an exam, for
example (assuming you don't know enough nor care about what's happening,
and can keep calm).

-   When connecting to a computer through USB, instead of the usual
    dialog asking the user to select a mode, a "Receiving..." screen,
    not usually accessible, appears. This is the color-version
    equivalent of the screen fx-9860G users see when connecting to a
    computer through USB. The computer, if a Windows machine configured
    to look for drivers, will look for CESG502 drivers, the same used
    for communication with a fx-9860G.

    Presumably, in this mode the calculator is listening for *Protocol
    7.00* commands, which, eventually along with its command 1 subtype
    56 (*Upload and Run*), can be used to recover a damaged bootloader.

-   OS updates using the official update bundles might not work. The
    updater will send a binary, presumably through *Upload and Run*,
    that in normal conditions is used to retrieve information about the
    calculator and the current OS, as well as receive and flash the new
    OS when proceeding. The information the updater wants to retrieve is
    in the first flash sector which, along with the bootloader, may be
    damaged, meaning this initial step can fail. In that case (the only
    studied one, so far), an error message will be shown on the computer
    and the calculator will not reboot or show any other change on
    screen. The updater payload doesn't respond to keyboard
    interaction.

    Further executing the updater bundle will do nothing as it cannot
    find the calculator in the correct state for receiving the updater
    payload (it doesn't expect it to be running the payload already).
    The only way to exit from a binary sent through *Upload and Run* is
    rebooting the calculator, which likely means killing the calculator
    as the bootloader is damaged.
