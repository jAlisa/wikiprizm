---
revisions:
- author: Balping
  comment: "Created page with \u201CLibrary which contains the needed headers\nfor\
    \ Casio PRIZM development. You can download the newest version\nfrom \\[https://github.com/Jonimoose/libfxcg\
    \ here\\] \\<!\u2013Please add\nmore\u2026\u201D"
  timestamp: '2012-08-28T12:04:07Z'
title: Libfxcg
---

Library which contains the needed headers for Casio PRIZM development.
You can download the newest version from
[here](https://github.com/Jonimoose/libfxcg)
