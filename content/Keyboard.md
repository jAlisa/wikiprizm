---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-11T14:11:56Z'
title: Keyboard
---

Human input into the Casio Prizm [OS]({{< ref "OS_Information/" >}}) and
[bootloader]({{< ref "CASIOABS.md" >}}) takes place through the
keyboard. The only other input devices on the Prizm are the 3-pin link
port, the USB port and the battery voltage meter, which obviously do not
count as human interface devices (despite the fact that the OS can
detect when a USB or 3-pin cable is connected, their primary purpose is
certainly not human interaction).

Representation of the Prizm keyboard:

[1](http://s.lowendshare.com/10/1416339673.716.pkbd.png)

## General aspects {#general_aspects}

The keyboard is made of 50 dome-switch keys in the front of the
calculator's body, below the screen. The pinhole RESTART button on the
back can be considered part of the keyboard, too, plus some hidden
contacts only accessible when disassembling the calculator.

There are two key modifiers, Shift and Alpha, which allow for selecting
one of four keyboard modes: no modifier (default), Shift, Alpha and
Alpha-lock (where most keys behave as if Alpha had been pressed before
them). In specific parts of the user interface, more keyboard modes are
allowed: when editing text, pressing Shift then 8 (CLIP) will enter a
text-selecting mode where users can copy or cut text into the clipboard,
and when the A\<-\>a label appears above one of the function keys, users
can also enter the lowercase Alpha mode (press Alpha, then the A\<-\>a
function key) or the lowercase Alpha-lock mode (press Shift then Alpha,
and use A\<-\>a to switch between lower- and upper-case text entry).

Alpha modes, by default, only allow the insertion of uppercase letters.
Even on screens where lowercase characters are allowed, the Alpha and
Alpha-lock mode always start in upper-case mode.

Keyboard modifiers are provided by software; there are no changes to the
electric signals produced by the keyboard when the keyboard is in a mode
other than the default.

## Technical information {#technical_information}

At what seems to be the lowest level, the keyboard port is exposed on
address 0xA44B0000 as a 16-bit unsigned value, with structure compatible
with that of the SH7724. Other addresses with values related to the
keyboard exist, but their exact meaning and value is unknown (Simon's
docs, fx-MPUs page, look for "Available information about the
7305-Ports").

The lowest level keyboard syscalls seem to take the value from this
register and return matrix values based on it, which are 16-bit unsigned
values (*unsigned short*) too. Some other syscalls use a key buffer and
interrupts to return their values. Some syscalls provide key debouncing,
and others don't.

The relationship between keys and the matrix codes produced by some
syscalls can be found [here]({{< ref "Keycode_Reference.md" >}}).

The highest-level keyboard syscall, which apparently also does a lot of
things unrelated to the keyboard, is
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}).

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

## Usability guidelines {#usability_guidelines}

Here are some tips regarding how keyboard input should be treated inside
add-ins, including during text or number entry. Of course, not all of
these make sense if your add-in is a game, namely those regarding
modifiers. The ones about the MENU and EXIT keys are always important,
though.

*Read the [original discussion
topic](http://www.cemetech.net/forum/viewtopic.php?t=7468) on the
Cemetech forum*

-   Make modifier keys behave as throughout the rest of the OS. This
    means, do not do things like require the user to press Shift then
    the key to modify simultaneously, as if it were a computer keyboard.
    There's a reason why it was designed to let you press Shift/Alpha
    first and then the key: it allows for easier single-hand operation
    and, since the keyboard is small, it lowers the chance that keys
    will accidentally be pressed, especially in the case of people with
    big fingers.

-   Even though having Alpha always starting in uppercase mode is mostly
    a nuisance, this is the way Casio designed it, it's been like this
    for many generations of Casio calculators, and after some time users
    tend to get used to it. There's also the chance that Casio may later
    introduce a setting to change this, which would affect all the code
    using [GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}). Please do
    not change the Alpha-is-uppercase-by-default behavior in your
    add-ins unless you have a valid reason.

-   By default, text input is in insert mode. DEL is always a backspace
    key.

-   Do not disable the MENU key, much less assign it to other function,
    unless you have a very specific reason. MENU is for exiting to the
    main menu only, as can be seen throughout the OS, except on the few
    situations where the key is disabled, such as when optimizing
    storage memory or running the first-run setup. Not completely
    related to the keyboard, but your add-in should call GetKey in a
    loop, not return to the caller. See [When to call
    GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) for more details.

-   Similarly, EXIT is to back out of a window/screen/menu/operation,
    and QUIT is to go back all to the main add-in screen, much like
    restarting the add-in (it's as if the user repeatedly pressed exit
    to get there).

-   AC/on should be used to break from certain conditions and
    operations, such as program execution, clipping, and the catalog.

See
[Addin_Usability_Guidelines]({{< ref "Addin_Usability_Guidelines.md" >}})
for other non-keyboard-related guidelines.
