---
bookCollapseSection: true
revisions:
- author: Tari
  comment: "Created page with \u201CSome of the syscalls in the miniSDK have\nuselessly\
    \ verbose names, so they have been renamed in libfxcg. For\nsource compatibility\
    \ with old programs, the old names may be defin\u2026\u201D"
  timestamp: '2012-05-18T16:18:25Z'
title: Renamed_Syscalls
---

Some of the syscalls in the miniSDK have uselessly verbose names, so
they have been renamed in libfxcg. For source compatibility with old
programs, the old names may be defined at build time. See [SDK
Compatibiilty](SDK_Compatibiilty) for details.
