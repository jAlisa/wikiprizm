---
title: DisableGetkeyToMainFunctionReturn
---

## Synopsis

**Header:** fxcg/keyboard.h
**Syscall index:** 0x1EA7
**Function signature**: `void DisableGetkeyToMainFunctionReturn(void)`

Sets the current behavior of [GetKey]({{< ref "Keyboard/GetKey.md" >}}) so that
pressing MENU will not return to the Main Menu.

## Comments

[GetGetkeyToMainFunctionReturnFlag]({{< ref GetGetkeyToMainFunctionReturnFlag.md >}})
can be used to determine whether the MENU will return to the Main Menu, and
[EnableGetkeyToMainFunctionReturnFlag]({{< ref EnableGetkeyToMainFunctionReturn.md >}})
can be used to re-enable this.
