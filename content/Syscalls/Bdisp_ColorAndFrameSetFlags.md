---
revisions:
- author: ProgrammerNerd
  comment: "Created page with \u201C{{Syscall \\| name =\nBdisp_ColorAndFrameSetFlags\
    \ \\| index = 0x0920 \\| signature =\nunsigned short Bdisp_ColorAndFrameSetFlags(int\
    \ colorMode,int\nframeColor); \\| header = fxcg/display\u2026\u201D"
  timestamp: '2015-02-22T23:42:44Z'
title: Bdisp_ColorAndFrameSetFlags
---

## Synopsis

**Header:** fxcg/display.h *(Not yet in
[libfxcg](https://github.com/Jonimoose/libfxcg))*\
**Syscall index:** 0x0920\
**Function signature:** unsigned short Bdisp_ColorAndFrameSetFlags(int
colorMode,int frameColor);

Calls
[Bdisp_FrameAndColor]({{< ref "Syscalls/Bdisp_FrameAndColor.md" >}})
with an action value of 3. This means you will need to call the
[Bdisp_FrameAndColor]({{< ref "Syscalls/Bdisp_FrameAndColor.md" >}})
function with an action value of one to see the results of the newly set
flags.\

## Parameters

**colorMode** Nonzero if 8 color mode should be disabled. Zero to enable
8 color mode. **frameColor** Nonzero to draw a white frame, zero to draw
a black frame.\

## Returns

See
[Bdisp_FrameAndColor]({{< ref "Syscalls/Bdisp_FrameAndColor.md" >}})\

## Comments

Here is a disassembly of the function:

    Bdisp_ColorAndFrameSetFlags:
            tst r4, r4
            extu.b  r5, r2
            bf/s    r4NonZero
            mov #16, r5     ! If r4 is nonzero set r5 to 16 to disable 8-color mode
            mov #0, r5      ! Otherwise set r5 to zero to enable 8 color mode

    r4NonZero:
            tst r2, r2
            bf  r2NonZero
            mov #1, r0
            or  r0, r5      ! Draw a black frame instead of a white one

    r2NonZero:
            bra Bdisp_FrameAndColor
            mov #3, r4      ! Set flags only, no actions other than flag setting are taken by the funciton
    ! End of function Bdisp_ColorAndFrameSetFlags
