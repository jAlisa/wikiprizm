---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:08:32Z'
title: APP_SYSTEM_LANGUAGE
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1E0D\
**Function signature:** void APP_SYSTEM_LANGUAGE(int opt);

Opens the "Message Language" screen that shows in the start-up wizard
and in the System menu (when F3 is pressed).\

## Parameters

-   **opt** - Controls whether the screen should behave like in the
    wizard (1), or like in the System menu (0).\

## Comments

The settings for the language actually change when an option is
selected.

If the value of the parameter is zero, the syscall returns when the user
presses EXIT. If the value of the parameter is 1, it returns when the
user presses F6.
