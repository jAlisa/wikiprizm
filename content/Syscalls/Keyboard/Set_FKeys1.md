---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:20:18Z'
title: Set_FKeys1
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x012B\
**Function signature:** void Set_FKeys1(unsigned int p1, unsigned int\*
P2)

Remaps the function-key (F1 to F6) codes. Under normal circumstances
this syscall shouldn't be invoked.\

## Parameters

See [#Comments](#Comments) for an explanation.\

## Comments

If P2 is null, the short at 0x8804D712 is set to 0xFFFF.

If P1 is between 0 and 289 and P2 is null, 0x8804D710 is stored to a
pointer-array starting at 0x88091280. The index of the item is P1\*4.

If P1 is between 0 and 289 and P2 is not null, P2 is stored to a
pointer-array starting at 0x88091280. The index of the item is P1\*4.

Simon Lothar's document indicates that calling this syscall is only
necessary if an add-in is run from RAM.
