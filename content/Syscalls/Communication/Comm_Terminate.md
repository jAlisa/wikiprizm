---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T15:36:38Z'
title: Comm_Terminate
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x13F1\
**Function signature:** int Comm_Terminate(unsigned char subtype)

Sends a *Protocol 7.00* terminate packet of the given subtype. Not to be
confused with
[Comm_Close]({{< ref "Syscalls/Communication/Comm_Close.md" >}}).\

## Parameters

-   **subtype** - Subtype of the packet to send:
    -   0 is "default" and makes the connected calculator exit
        displaying "Complete!",
    -   1 is for user-requested termination and makes the connected
        calculator exit displaying "Terminated!",
    -   2 is for timeout-caused termination and makes the connected
        calculator exit displaying "Receive ERROR",
    -   3 is for termination on overwrite request and makes the
        connected calculator exit receive mode silently.\

## Returns

The meaning of the return value is not yet known.\

## Comments

For more information on *Protocol 7.00*, see
[fxReverse.pdf](http://tny.im/dl/fxReverse2x.pdf).
