---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:32:48Z'
title: Communication
---

These are [syscalls]({{< ref "syscalls.md" >}}) used for higher level
communication using *Protocol 7.00* or similar, through the 3-pin
[serial]({{< ref "Technical_Documentation/serial.md" >}}) port or
[USB]({{< ref "USB_Communication.md" >}}). Most are related to the
built-in "Link" app.

For lower level communication through the 3-pin serial port, that is not
bound to a specific protocol, see the [Serial
category]({{< ref "Syscalls/Serial/" >}}). Note that some of the
syscalls in this category are a plain wrapper around the Serial
syscalls, but only if the chosen channel is the serial port.
