---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-11T12:26:02Z'
title: CMT_Delay_micros
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x11D6\
**Function signature:** void CMT_Delay_micros(int microseconds_delay)

Holds program execution by a given amount of microseconds.\

## Parameters

-   **microseconds_delay** - delay during which to hold program
    execution, in microseconds.\

## Comments

According to Simon Lothar's documentation, this syscall works based on
the 7730 CMT.
