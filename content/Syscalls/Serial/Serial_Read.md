---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T10:25:49Z'
title: Serial_Read
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BBA\
**Function signature:** int Serial_Read(unsigned char \*out, int sz,
short \*count)

Reads bytes from the serial receive buffer.\

## Parameters

-   **out** - Buffer to write received data to.
-   **sz** - Maximum number of bytes to read.
-   **count** - Output parameter receiving the number of bytes actually
    read.\

## Returns

-   0 if successful,
-   1 if the receive buffer is empty,
-   3 if the serial channel is not
    [open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}).

If successful, the actual number of bytes read is returned in *count*.\

## Comments

The receive buffer can hold up to 1024 bytes. Its behavior on overflow
is unknown.

Originally documented as Serial_ReadNBytes.
