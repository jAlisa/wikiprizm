---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T10:38:49Z'
title: Serial_PollRX
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BBF\
**Function signature:** int Serial_PollRX(void)

Gets the amount of bytes available in the serial receive buffer\

## Returns

The number of bytes available in the receive buffer.
