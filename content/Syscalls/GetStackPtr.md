---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = GetStackPtr \\| header\n\
    = fxcg/system.h \\| index = 0x1A2C \\| signature = void\\*\nGetStackPtr(void)\
    \ \\| synopsis = Gets a pointer to the current top of\nthe stack. \\| r\u2026\u201D"
  timestamp: '2014-08-01T20:51:54Z'
title: GetStackPtr
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1A2C\
**Function signature:** void\* GetStackPtr(void)

Gets a pointer to the current top of the stack.\

## Returns

A pointer to the current top of the stack, allowing for measuring stack
usage.
