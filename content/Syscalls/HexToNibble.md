---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-31T17:46:09Z'
title: HexToNibble
---

## Synopsis

**Header:** fxcg/misc.h\
**Syscall index:** 0x1343\
**Function signature:** void HexToNibble(unsigned char value, unsigned
char\* result)

Converts the hexadecimal representation of a nibble to a nibble.\

## Parameters

-   **value** - byte containing the hexadecimal letter that represents
    the nibble.
-   **result** - pointer to `unsigned char` that will hold the
    conversion result.
