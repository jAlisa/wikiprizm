---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = PrintGlyph \\| index =\n\
    0x0238 \\| signature = void PrintGlyph(int x, int y, unsigned char\\*\nglyph,\
    \ int mode_flags, int color, int back_color, int P7) \\| header\n=\u2026\u201D"
  timestamp: '2014-07-29T16:20:10Z'
title: PrintGlyph
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0238\
**Function signature:** void PrintGlyph(int x, int y, unsigned char\*
glyph, int mode_flags, int color, int back_color, int P7)

Draws a single glyph (character) to VRAM.\

## Parameters

-   **x** - The x coordinate (in pixels) of the upper left corner of the
    character.
-   **y** - The y coordinate (in pixels) of the upper left corner of the
    character. Incremented by 24 unless mode_flags has the bit
    corresponding to 0x40 set.
-   **glyph** - Pointer to the glyph.
-   **mode_flags** - Bitmask. See [PrintXY's note
    2]({{< ref "Syscalls/PrintXY.md" >}}). In addition, if 0x40, allows
    for drawing into the status area.
-   **color** - The RGB565 color of the characters, use the COLOR\_\*
    constants from color.h
-   **back_color** - The RGB565 color of the background, use the
    COLOR\_\* constants from color.h
-   **P7** - meaning unknown.
