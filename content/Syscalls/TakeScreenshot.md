---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-03T21:51:07Z'
title: TakeScreenshot
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x17E6\
**Function signature:** void TakeScreenshot(void)

Screenshots the current VRAM contents and opens a dialog where the user
can save the screenshot to a picture file, in the format specified in
the Link menu (G3P or BMP), or discard the screenshot by exiting the
dialog. This is the same behavior as when CAPTURE (Shift+7) is pressed
while [GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) is running.\

## Comments

The existence of parameters or return values is yet to be studied.
