---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = BoxYLimits \\| index =\n\
    0x17FB \\| signature = void BoxYLimits(int lines, int*top,\nint*bottom) \\| header\
    \ = fxcg/display.h \\| parameters = \\*\n\u2019\u2018\u2019lines\u2019\u2019\u2019\
    \ - amount of\u2026\u201D"
  timestamp: '2014-07-29T12:58:19Z'
title: BoxYLimits
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x17FB\
**Function signature:** void BoxYLimits(int lines, int\*top,
int\*bottom)

The function of this syscall is not confirmed, but it appears to allow
for retrieving the limits for the contents of a message box with a
certain amount of lines.\

## Parameters

-   **lines** - amount of lines the message box has.
-   **top** - pointer to an integer which will hold the upper limit for
    content.
-   **bottom** - pointer to an integer which will hold the lower limit
    for content.
