---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Bfile_Name_MatchMask\n\\\
    | header = fxcg/file.h \\| signature = int\nBfile_Name_MatchMask(const short\\\
    * mask, const short\\* filename) \\|\nindex = 0x1DDA \\| parameters =\u2026\u201D"
  timestamp: '2014-07-30T11:52:27Z'
title: Bfile_Name_MatchMask
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DDA\
**Function signature:** int Bfile_Name_MatchMask(const short\* mask,
const short\* filename)

Checks whether a certain filename matches the *mask* provided. Useful
for filtering files, after using a less restrictive filter with
[Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}}) and
related syscalls.\

## Parameters

-   *const short\** **mask** - Pointer to a 16 bit string the filename
    will be matched against.
-   *const short\** **filename** - Pointer to a 16 bit string containing
    the filename to match.\

## Returns

1 if **filename** matches the **mask**, or 0 if not.
