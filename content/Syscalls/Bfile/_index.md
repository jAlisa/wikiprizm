---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-08-03T22:26:51Z'
title: Bfile
---

Bfile functions are used to operate on the Prizm's [storage
memory]({{< ref "File_System.md" >}}).

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.

The OS supports up to 16 simultaneously open files and file finding
handles, but strange behavior (most likely a OS bug) occurs if files
other than the one in the first handle slot are located in directories
(other than the root). This is why the [file copying function of the
Utilities
add-in](https://github.com/gbl08ma/utilities/blob/32d0e0d379a7ddb93501a22eff6340b3f5fb7b96/src/fileProvider.cpp#L256),
which opens both the source and the destination file at once, uses a
temporary file in the filesystem root as the destination file, and only
[moves]({{< ref "Syscalls/Bfile/Bfile_RenameEntry.md" >}}) it to the
intended correct destination path in the end.
