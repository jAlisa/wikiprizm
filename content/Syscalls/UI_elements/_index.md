---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:36:34Z'
title: UI_elements
---

This is a list of [syscalls]({{< ref "syscalls.md" >}}) that help with
drawing common UI elements, such as message boxes, scroll bars and
progress bars, or are otherwise related to their operation. [Syscalls
that aid in text and expression
editing]({{< ref "Syscalls/Text_and_expression_editing/" >}}) can be
considered to fit this category, but are not included here.
