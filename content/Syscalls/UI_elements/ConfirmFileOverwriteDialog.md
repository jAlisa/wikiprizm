---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:02:51Z'
title: ConfirmFileOverwriteDialog
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1802\
**Function signature:** int ConfirmFileOverwriteDialog(unsigned short\*
filename)

Shows a file overwrite confirmation dialog for the specified filename.\

## Parameters

-   **filename** - 16 bit string that will be shown as the file name to
    overwrite.\

## Returns

The return values for this function have not yet been verified, but
presumably they are related to the user's answer to the confirmation.
