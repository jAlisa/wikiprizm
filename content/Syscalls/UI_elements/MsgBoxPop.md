---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:07:00Z'
title: MsgBoxPop
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x17F9\
**Function signature:** void MsgBoxPop(void)

Releases the resources allocated by
[MsgBoxPush]({{< ref "Syscalls/UI_elements/MsgBoxPush.md" >}}). Must be
called after displaying a message box with MsgBoxPush because otherwise
the system will crash.
