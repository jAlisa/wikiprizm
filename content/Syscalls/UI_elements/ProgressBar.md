---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:07:34Z'
title: ProgressBar
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x180E\
**Function signature:** void ProgressBar(int current, int max)

Displays a progress bar on a message box. Similar to
[ProgressBar0]({{< ref "Syscalls/UI_elements/ProgressBar0.md" >}}).\

## Parameters

-   **current** - the current value (call with this value set to zero
    for initialization).
-   **max** - the maximum value **current** is expected to contain.\

## Comments

As with
[ProgressBar0]({{< ref "Syscalls/UI_elements/ProgressBar0.md" >}}), this
syscall should be called with **current** as zero for initialization,
and since it calls
[MsgBoxPush]({{< ref "Syscalls/UI_elements/MsgBoxPush.md" >}}), the
message box must be closed with
[MsgBoxPop]({{< ref "Syscalls/UI_elements/MsgBoxPop.md" >}}) in order to
free resources.
