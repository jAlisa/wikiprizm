---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2015-02-11T12:39:21Z'
title: RTC
---

This is a list of syscalls that operate on the
[Real-Time_Clock]({{< ref "Technical_Documentation/Peripherals/Real-Time_Clock.md" >}}).
