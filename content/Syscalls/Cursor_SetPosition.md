---
revisions:
- author: ProgrammerNerd
  comment: Fix copy and paste error.
  timestamp: '2015-02-15T17:20:09Z'
title: Cursor_SetPosition
---

## Synopsis

**Header:** fxcg/display.h *(Not yet in
[libfxcg](https://github.com/Jonimoose/libfxcg))*\
**Syscall index:** 0x01F1\
**Function signature:** int Cursor_SetPosition(int x, int y)

Sets the cursor position for
[Print_OS]({{< ref "Syscalls/Print_OS.md" >}}).\

## Parameters

-   **x** Must be in range of \[0,20\]
-   **y** Must be in range of \[0,7\]\

## Returns

0 on failure, 1 on success.\

## Comments

This function like [locate_OS]({{< ref "Syscalls/locate_OS.md" >}}) does
bounds checking. It is better to use this function as opposed to
[locate_OS]({{< ref "Syscalls/locate_OS.md" >}}) to avoid a duplication
of bounds checking.

    Cursor_SetPosition:
        cmp/pz  r4
        bf      Cursor_SetPosition_OutOfBounds
        mov     #21, r2
        cmp/ge  r2, r4
        bt      Cursor_SetPosition_OutOfBounds
        cmp/pz  r5
        bf      Cursor_SetPosition_OutOfBounds
        mov     #8, r2
        cmp/ge  r2, r5
        bf      loc_8004E744

    Cursor_SetPosition_OutOfBounds:
        rts
        mov     #0, r0
    ! ---------------------------------------------------------------------------

    loc_8004E744:
        mov.l   #CursorXpos, r1
        mov     #1, r0
        mov.l   #CursorYpos, r6
        mov.l   r4, @r1
        rts
        mov.l   r5, @r6
    ! End of function Cursor_SetPosition
