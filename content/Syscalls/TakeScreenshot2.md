---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = TakeScreenshot2 \\|\nheader\
    \ = fxcg/system.h \\| index = 0x17E7 \\| signature = void\nTakeScreenshot2(void)\
    \ \\| synopsis = Has apparently no difference\nfrom TakeScre\u2026\u201D"
  timestamp: '2014-08-03T21:49:31Z'
title: TakeScreenshot2
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x17E7\
**Function signature:** void TakeScreenshot2(void)

Has apparently no difference from
[TakeScreenshot]({{< ref "Syscalls/TakeScreenshot.md" >}}), but this is
not certain since parameters and return values are yet to be studied for
both syscalls.
