---
revisions:
- author: Balping
  comment: cg50 address caching
  timestamp: '2018-05-03T15:22:51Z'
title: GetVRAMAddress
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01E6\
**Function signature:** void \*GetVRAMAddress(void)\

## Returns

A pointer to the system's
[VRAM]({{< ref "Technical_Documentation/Display.md" >}}).\

## Comments

While this function currently always returns 0xA8000000 on CG- 10/20
models, and 0xAC000000 on CG50. Use this syscall for portability or with
detecting the calculator model.

If speed is a concern, it will be slightly faster to use a hard-coded
(preprocessor define) address for VRAM. This is a bad idea, however, as
portability to the CG50 is very simple if the syscall is used.
Alternatively, you could write two functions based one the return result
with hardcoded values for faster performance. Another option is to use a
global pointer variable for caching the address. This way,
GetVRAMAddress needs to be called only once, upon initialisation.
