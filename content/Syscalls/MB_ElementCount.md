---
revisions:
- author: ProgrammerNerd
  timestamp: '2015-02-15T02:26:08Z'
title: MB_ElementCount
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1163\
**Function signature:** int MB_ElementCount(const char\* buf)

Counts the number of characters, as printed on screen, of a [multi-byte
string]({{< ref "Multi-byte_strings.md" >}}).\

## Parameters

-   **buf** - pointer to the string to measure.\

## Returns

The number of characters in the string, as printed by multi-byte-aware
syscalls.\

## Comments

Here is a disassembly of the function.

    ! =============== S U B    R O U T I N E =======================================


    MB_ElementCount:
            tst r4, r4      ! Is r4 NULL?
            bt  MB_ElementCount_exit ! If yes, branch.
            mov #0xFFFFFFE5, r1
            mov #0xFFFFFFE6, r7
            extu.b  r1, r1      ! r1=229
            mov #0, r5
            bra loc_801A3478
            extu.b  r7, r7      ! r7=230
    ! ---------------------------------------------------------------------------

    MB_ElementCount_charLoop:
            cmp/eq  #0x7F, r0
            bt  loc_801A3472
            cmp/eq  r1, r0
            bt  loc_801A3472
            cmp/eq  r7, r0
            bt  loc_801A3472
            mov #0xFFFFFFE7, r2
            extu.b  r2, r2
            cmp/eq  r2, r0
            bt  loc_801A3472
            mov #0xFFFFFFF7, r2
            extu.b  r2, r2
            cmp/eq  r2, r0
            bt  loc_801A3472
            mov #0xFFFFFFF9, r2
            extu.b  r2, r2
            cmp/eq  r2, r0
            bf  loc_801A3474

    loc_801A3472:
            add #1, r4

    loc_801A3474:
            add #1, r4
            add #1, r5

    loc_801A3478:
            mov.b   @r4, r2
            extu.b  r2, r0
            tst r0, r0
            bf  MB_ElementCount_charLoop ! Branch if the character has a non-zero value.
            rts
            mov r5, r0
    ! ---------------------------------------------------------------------------

    MB_ElementCount_exit:
            rts
            mov #0, r0
    ! End of function MB_ElementCount

## Example

The following example shows how a multi-byte string can take a memory
space that's twice as large as the number of printed characters, and
demonstrates how to count both size in bytes and graphical size:


    const char*MBstring="\xe6\x92\xe6\x93\xe6\xa5"
    int bytelen = strlen(MBstring);
    // bytelen has a value of 6
    int elemlen = MB_ElementCount(MBstring);
    // elemlen has a value of 3

    locate_OS(2,3);
    Print_OS(MBstring, 0, 0); // will print three symbols on screen: an arrow up, an arrow down and a square.

Another example, which mixes standard and multi-byte characters:


    const char*MBstring="Empty box: \xe6\xa5";
    int bytelen = strlen(MBstring);
    // bytelen has a value of 13
    int elemlen = MB_ElementCount(MBstring);
    // elemlen has a value of 12

    locate_OS(2,3);
    Print_OS(MBstring, 0, 0); // will print "Empty box: " followed by a square.
