---
revisions:
- author: Ahelper
  comment: Added some return values.
  timestamp: '2014-11-23T04:38:16Z'
title: Timer_Stop
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x08DC\
**Function signature:** int Timer_Stop(int InternalTimerID)

Stops a [timer]({{< ref "/OS_Information/Timers.md" >}})
[installed]({{< ref "Syscalls/Timers/Timer_Install.md" >}}) at the given
slot.\

## Parameters

-   **InternalTimerID** - slot of the timer to stop.\

## Returns

Returns 0 if the timer was uninstalled, -2 if **InternalTimerID** wasn't
installed.
