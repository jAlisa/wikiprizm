---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T16:19:53Z'
title: PrintCXY
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0239\
**Function signature:** void PrintCXY(int x, int y, const char \*cptr,
int mode_flags, int P5, int color, int back_color, int P8, int P9)

Similar to [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}), displays a line
of text at the given position in pixels.\

## Parameters

-   **x** - The x coordinate (in pixels) of the upper left corner of the
    first character
-   **y** - The y coordinate (in pixels) of the upper left corner of the
    first character
-   **cptr** - The string to display. The first two characters are not
    ignored, in contrast to
    [PrintXY]({{< ref "Syscalls/PrintXY.md" >}}).
-   **mode_flags** - Bitmask. See [PrintXY's note
    2]({{< ref "Syscalls/PrintXY.md" >}})
-   **P5** - unknown, set this to -1
-   **color** - The RGB565 color of the characters, use the COLOR\_\*
    constants from color.h
-   **back_color** - The RGB565 color of the background, use the
    COLOR\_\* constants from color.h
-   **P8** - unknown, set this to 1
-   **P9** - unknown, set this to 0\

## Example

    PrintCXY(44, 56, "Hello world!!!", TEXT_MODE_NORMAL, -1, COLOR_BLACK, COLOR_YELLOW, 1, 0 );
