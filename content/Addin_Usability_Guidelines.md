---
revisions:
- author: Heath
  comment: 'Fix info about areas that are drawn over (applies to the

    CG50, at least, not sure about the CG10/20)'
  timestamp: '2021-11-28T14:41:22Z'
title: Addin_Usability_Guidelines
---

Throughout the [OS]({{< ref "OS_Information/" >}}) and official add-ins,
Casio maintains some amount of coherency regarding the user experience.
This includes not just the appearance of the graphical interface, but
also the way the user interacts with it and the way it responds. Whether
the User Experience is effectively good or bad is a whole other subject,
and add-ins can definitely try to improve on it. However, they should
not provide a experience that's worse than that of the OS, and when
improving, they should do so in ways that do not break user habits,
namely in terms of how to navigate the interface, turn off the
calculator, switch apps, etc.

## Icon Guidelines {#icon_guidelines}

*Read the [original discussion
topic](http://www.cemetech.net/forum/viewtopic.php?t=6211) on the
Cemetech forum*

Icons are a part of Prizm add-ins that are usually considered
unimportant. They don't affect the add-in itself, but it's still a good
idea to create a nice set of icons for any program. Users will see it
even when they aren't running the program, and if the Main Menu is full
of icons that don't adhere to a few guidelines, navigating the menu can
be confusing.

### Style

The object that's the main focus of the icon shouldn't take up all of
the 92x64 pixels that it has access to. It should generally be a bit
smaller since the background and other elements also need a bit of
space. The official icons from Casio are somewhat colorful, a bit
3D-looking, have shadows and are slightly translucent. You can follow
the style of these if you want to, or create your own kind of icon. 2D
sprites usually work fine if they aren't too similar to the backgrounds.

**Example icons:**

\[<https://wikiprizm%5D-images.heathmitchell1.repl.co/prizmicons.png>

### Selected/Unselected

In all add-ins, two icons must be provided - the selected icon, and the
unselected icon. The two icons should show the same objects with
different backgrounds - not objects that are completely different from
each other or the same objects with minor variations. There is one
exception, though. When placing text in the icon (this should only be
done in special cases, not for writing the name of the add-in!), the
text in the unselected icon should be white with a black outline and the
text in the selected icon should be black with a white outline.
Run-Matrix and Equation are examples of this. The unselected icon needs
to have a pure black background, and the selected icon needs to have
this background:

\[<https://wikiprizm%5D-images.heathmitchell1.repl.co/prizmempty.png>

If no custom icon has been made for your program, the background with
nothing on top of it should be used for the selected icon. Never make
the selected icon pure black!

### Areas drawn by the OS {#areas_drawn_by_the_os}

The Prizm OS is a bit unusual when it comes to icons, as it draws on top
of certain areas. Those areas are marked in red here:

\[<https://wikiprizm%5D-images.heathmitchell1.repl.co/prizmguidelines.png>

The square on the top right area of the icon will be unseen (unless the
user has more than 26 addins, and the letters from A-Z have already been
assigned to other addins), and anything important shouldn't be placed
there. However, hiding defects or other odd things there is not
recommended, since some users might have over 26 addins or use something
other than the OS to view the icons. The larger area on the bottom is
where the name of the add-in will be placed. The main focus of the icon
shouldn't be under it, but the area won't be completely covered, so
things that aren't very important can be placed there. For example, a
small part of the ruler in the icons of Conversion and Geometry is
placed there, and an unimportant part of one of the images in the
Picture Plot icon is also placed there. The name of the add-in should
never be included in the icon since the OS takes care of including it.

### For projects in development {#for_projects_in_development}

If you have an in-dev project and can't make an icon, there is
salvation!

KermMartian made these icons for in-dev projects:

**Unselected**

[1](http://www.cemetech.net/img/icon/indev_u.png)

**Selected**

[2](http://www.cemetech.net/img/icon/indev_s.png)

## Key Usage {#key_usage}

See [Keyboard - Usability Guidelines]({{< ref "Keyboard.md" >}}).

## Clock control {#clock_control}

As discussed in [an earlier
thread](http://www.cemetech.net/forum/viewtopic.php?t=7844), until
recently most Prizms could safely overclock to around 95 MHz, but *not
all*. More recently, the development of a tool called
[Ptune2](http://www.cemetech.net/forum/viewtopic.php?t=10870) made
overclocking to higher frequencies, in possibly more devices and in a
eventually safer way, possible. Rather than make assumptions that can
easily cause not just instability, but also an undesired increase of the
power consumption and higher hardware wear-out, any modification to the
system clocks should not be done without the user's permission or, at
the bare minimum, knowledge.

The best way to handle overclocking is to simply not do it, by either
[optimizing the program]({{< ref "Optimization_Tips.md" >}}), or
reducing the amount of processing required. Drawing things to VRAM tends
to be quite slow compared to computation, so optimizing the drawing in a
program can offer significant speedups.

If the additional speed offered by overclocking is indeed important, to
the point where the program will not work correctly without it, be sure
to notify the user before doing anything and allow for backing out.
There are some cases where this guideline could be safely ignored (such
as an add-in designed specifically to allow the user to modify the clock
settings), but in most cases the user should be notified before the
program does anything that could negatively impact the calculator's
stability (namely, overclocking).

In addition to keeping the user informed, an add-in that modifies the
clock settings should also revert to the previous (stock or not) clock
speed on exit by hooking the \[MENU\] key. Using
[SetQuitHandler]({{< ref "Syscalls/SetQuitHandler.md" >}}) to register a
function that reverts the clock speed back to the default one will work
fine in most cases.
