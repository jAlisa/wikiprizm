---
bookCollapseSection: true
revisions:
- author: Ahelper
  comment: Initial
  timestamp: '2012-07-31T06:33:08Z'
title: Tutorials
---

This page lists the tutorials available for the Prizm. These tutorials
do go in order, so you should refer to [the programming
portal]({{< ref "Prizm_Programming_Portal.md" >}}) for the tutorial
order.
