---
aliases:
- Bootloader
revisions:
- author: Gbl08ma
  comment: Add checksum info
  timestamp: '2015-03-17T22:02:13Z'
title: CASIOABS
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

CASIOABS is the name of the bootloader used on the Prizm and other Casio
calculators.

On the Prizm at least, it is located on the first 128KB sector of
[flash]({{< ref "flash.md" >}}) (at 0x80000000 or 0xA0000000; the OS
only starts on the second sector) and is the first code executed by the
CPU when it is powered up. This is the only bootloader, and if the first
flash sector is erased, the device is bricked - see [Behavior on
Damage](#Behavior_on_Damage) for more information. Getting the first
flash sector erased is not exactly hard, as it has no write protection -
it is no different from other flash sectors.

## Boot sequence {#boot_sequence}

-   The CPU starts executing at the beginning of the
    [flash]({{< ref "flash.md" >}}), 0xA0000000;
-   The first code executed sets up the status register, sets the module
    stop register 0 to 0x1000, the module stop register 2 to 0xFFFFFFFF
    and the cache control register to 0x800, and using the ICBI
    instruction invalidates the instruction cache as mandated by the
    architecture;
-   The [PFC](PFC) and the [RWDT](RWDT) are initialized;
-   The [CPG](CPG) is initialized: PLLCR is changed, the FLL is changed.
    The program will wait for the frequency change status register to
    indicate the frequency change has completed. FRQCRA is then changed,
    and the program again waits for the change to complete;
-   The [BSC](BSC) is initialized;
-   The Exception Event Register is checked and code will take different
    paths depending on its value;
-   ...TODO

(after this, and after all the hardware initialization and check for
special key combinations, presumably the bootloader checks for the
string CASIOWIN, computes the OS checksum and sees if it matches with
the recorded one, and if not, enters OS emergency update mode)

## Included functionality and menus {#included_functionality_and_menus}

The bootloader includes, at least, one OS recovery method and one menu
for erasing certain flash areas. Information on how to access this
functionality can be found
[here]({{< ref "Secret_Key_Combinations.md" >}}).

## Checksum

The bootloader has a checksum at address 0xA001FFFC, which is the sum of
the bytes in the address range 0xA0000000 to 0xA00002FF plus the sum of
the bytes in the address range 0xA0000340 to 0xA001FFBF. The checksum is
verified by the OS and possibly the bootloader itself, and in case of a
mismatch special behavior will be triggered, as described in the
following sections.

## OS Behavior on Damage {#os_behavior_on_damage}

If the OS (post-boot, of course) detects a damaged bootloader, it will
show [special behavior]({{< ref "Error_handling.md" >}}).

## Bootloader Behavior on Damage {#bootloader_behavior_on_damage}

It is suspected that, at least on earlier models (fx-9860G), the
bootloader (which used more than one of some smaller flash sectors)
performs flash writes into its own sectors under certain conditions,
then reboots. It is not yet clear whether these writes really happen,
nor when, nor for what, much less if the Prizm bootloader also
self-modifies, but if such writes can actually occur, one possibility is
that the CASIOABS on such models attempts some sort of self-patching,
like inserting an unconditional jump to the emergency OS updater, in
case of integrity loss.

## Recovering From a Damaged Bootloader {#recovering_from_a_damaged_bootloader}

*Information in this section is purely speculative, because as far as
the community knows, no Casio Prizm has ever failed in a way where a
recovery could be attempted. Anyway, here are some ideas on what to do
if a recovery can be attempted.*

If the OS has booted, the "Receiving" mode described
[here]({{< ref "Error_handling.md" >}}) is certainly interesting and
useful. *Protocol 7.00* includes some commands that may be used to fix a
broken bootloader, like: Packet type 0x01 (Command), subtype "50"
("Flash image transfer"). This could possibly be used to transfer a full
flash image taken from a working calculator. Bonus points if it can also
upgrade the calculator model.

Even if this command is not supported in the listening implementation of
the protocol on the damaged calculator, there's also: Packet type 0x01
(Command), subtype "56" (*Upload and Run*), which is used to upload any
binary to a specified RAM address (this is what the updater bundles do
to send the payload). It should be possible, even if unpractical and
with reduced chance of success, to craft a binary that is made to run
from RAM, and can write a good CASIOABS from a good copy of it. This
copy could be stored on the payload itself (easier, but needs a bigger
payload and RAM area to run it), or it could be received through USB
(harder, requires that a implementation of the USB protocol be included
in the payload, but it's not impossible at least for Casio, because it's
what the OS updater does).

If the calculator failed in such a way that entering *Protocol 7.00* was
not possible (OS not booted, for example), one can attempt to reprogram
the bootloader in flash by using an adequate flash programmer.

------------------------------------------------------------------------

Special thanks to Simon Lothar for documenting the *Upload and Run*
functionality.
