---
revisions:
- author: Dr-carlos
  comment: Add a missing full stop.
  timestamp: '2022-03-17T10:27:58Z'
title: Mkg3a
---

mkg3a is a utility to combine icons and a binary into a .g3a file
executable on the Prizm. The project page for mkg3a is located
[here](http://www.taricorp.net/projects/mkg3a). If you installed mkg3a
from source yourself, it should have installed a manpage, which provides
all of the below information (run *man mkg3a*).

## Download

You can find the soure code of mkg3a [on
Bitbucket](https://gitlab.com/taricorp/mkg3a), but you will have to
compile it from source. Arch Linux users can also find it in [the
AUR](https://aur.archlinux.org/packages/mkg3a).

## Usage

*Note that mkg3a has been changed since the release of PrizmSDK 3. If
the commands listed don't work, please update your mkg3a*

The command line usage of mkg3a is in the form of *mkg3a \[OPTION\]
input-file \[output-file\]*. The input file should be a .bin file, as
emitted by gcc/ld with the Prizm linker script. The output file is a
.g3a file, the name of which is auto-generated from your input file name
(chop off '.bin', tack on '.g3a') if not specified. mkg3a accepts the
following options:

-   **-i** *(uns\|sel)*:*file* - This is used to specify the unselected
    and selected icons for when the addin is displayed in the Prizm's
    Main Menu. These images may be in .bmp or .png. In both cases, the
    images must be 24-bit RGB, and BMP files may not be compressed. They
    must also be the correct size (64 by 92 pixels). For icon design
    guidelines, refer to the [Addin Usability
    Guidelines]({{< ref "Addin_Usability_Guidelines.md" >}}).
-   **-n** *lc*:*name* - This is used to set the localized name of the
    addin. This is needed because addins contain the name in different
    languages and for different modes. If you omit *lc:* (such as *-n
    name*), then mkg3a will default to the basic name. You can specify
    as many language names as you want, however the last one specified
    for a given language will be used. The list of language codes are
    listed below:
    -   *en* - English name
    -   *es* - Spanish name.
    -   *de* - German name.
    -   *fr* - French name.
    -   *pt* - Portuguese name.
    -   *zh* - Chinese name.
    -   *basic* - (Default for mkg3a) Unlocalized name.
    -   *internal* - This appears to be used by the Prizm internally. It
        should generally not be set manually (mkg3a derives a reasonable
        value from the specified basic name).
-   **-v** - Displays the version info and copyright information

## Examples

Given *example.bin* as input, create *example.g3a*. The default name is
"James", and the French-localized name is "Jacques". Icons are foo.png
and bar.bmp, respectively.

`mkg3a -n James -n fr:Jacques -i uns:foo.png -i sel:bar.bmp example.bin`
