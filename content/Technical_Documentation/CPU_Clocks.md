---
revisions:
- author: Ahelper
  comment: 'Moved information specific to the CPG peripheral to its own

    page'
  timestamp: '2015-02-16T00:29:30Z'
title: CPU Clocks
---

*This page's documentation is not complete and is based off of preliminary
testing. There may be errors in the documentation or the naming conventions may
not be completely agreed upon.*

The processor used by the [Casio
Prizm]({{< ref "_index.md" >}}), a custom SuperH 4-A with no
FPU, utilizes four clocks to regulate commands. These clocks are known
as the CPU clock, the data bus clock, the SH clock and the peripheral
clock.

## CPU clock {#cpu_clock}

The CPU clock is the main clock used by the processor. It directly
controls the speed of the processor. The CPU clock itself is controlled
by a clock pulse generator (CPG). The CPG consists of a input clock
signal from a Crystal oscillator which is divided to achieve the
standard 58 MHz clock frequency of the Prizm. The circuit also contains
two internal phase locked loops (PLL circuits) which output data about
the state of the CPG.

## Peripheral clock {#peripheral_clock}

The peripheral clock operates similarly to the CPU clock and is derived
from the same CPG signal. It's used to operate peripheral modules and
also controls several registers with the CPG control unit, such as the
FRQCR (Frequency Control Register)

## Bus clock {#bus_clock}

This clock controls the speed of memory reads and other hardware related
functions. Its frequency is determined upon boot by the load on the CKIO
pin and can take the the value of either 1x or 4x the frequency of the
crystal oscillator. This value cannot be changed after startup. The bus
clock is also used as the signal source for the Peripheral and CPU
clocks. If the set division ratio correlates to a speed over 200Mhz,
memory operations may go haywire.

## SH clock {#sh_clock}

This clock, the SuperHyway clock, affects the speed of the SuperHyway
bus that connects most operations within the processor, such as
instruction fetching/sending, and IL data access.

## Important Quirks {#important_quirks}

Not all values specified by Renesas are invalid on the prizm. The prizm,
in fact, is already using invalid values.

-   The divisors must be in the correct ratio in order to function
    properly.
-   The CPU has a calculated max frequency of 500MHz. Going above that
    or close to 470MHz causes the hardware to lock up.
-   The values listed in the above table do **not** conform to the
    frequencies used by the CPU when calculated by Renesas' methods.

## Changing Clocks {#changing_clocks}

To change the clock frequencies, use the
[Clock_Pulse_Generator]({{< ref "Clock_Pulse_Generator.md" >}}).
