---
title: Direct Memory Access Controller
aliases:
  - /Direct_Memory_Access_Controller/
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

The SH7305 used on the Prizm includes a Direct Memory Access Controller
(DMAC), that can be used to perform high-speed transfers between
external devices, without stressing the SH4A core. The DMAC in the
SH7305 is identical to that in the SH7724, sharing the same base
address, 0xFE000820.

The [OS]({{< ref "/OS_Information/" >}}) uses the DMAC at least for updating
the LCD contents with the VRAM contents - see the
[Bdisp_PutDisp_DD]({{< ref "Syscalls/Bdisp_PutDisp_DD.md" >}}) syscall.
How to use the DMAC directly is understood, to a certain point: see
[Non-blocking_DMA]({{< ref "Useful_Routines/Non-blocking_DMA.md" >}}).
