---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Pie = == Description == This sets the\nstat\
    \ graph type to Pie graph. == Syntax ==\u2019\u2018\u2019Pie\u2019\u2019\u2019\
    \ == Example ==\nPie \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-21T00:21:22Z'
title: Pie
---

# Pie

## Description

This sets the stat graph type to Pie graph.

## Syntax

**Pie**

## Example

`Pie`
