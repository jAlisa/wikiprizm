---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= \u221A = == Description == This command\nreturns\
    \ the square root of the value. == Syntax ==\u2019\u2019\u2018\u221A\u2019\u2019\
    \u2019\u2018\u2019Value\u2019\u2019\n== Example == \u221A3 \\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-24T20:13:55Z'
title: "\u221A"
---

# √

## Description

This command returns the square root of the value.

## Syntax

**√***Value*

## Example

`√3`
