---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= CellMean( = == Description == This\ncommand\
    \ returns the mean value in a specified cell range. == Syntax\n==\u2019\u2018\u2019\
    CellMean(\u2019\u2019\u2019\u2018\u2019start_cell\u2019\u2018:\u2019\u2018end_cell\u2019\
    \u2019\u2019\u2019\u2018)\u2019\u2019\u2019 == Example ==\nCel\u2026\u2019"
  timestamp: '2012-02-16T00:04:15Z'
title: CellMean(
---

# CellMean(

## Description

This command returns the mean value in a specified cell range.

## Syntax

**CellMean(***start_cell*:*end_cell***)**

## Example

`CellMean(A3:C5)`
