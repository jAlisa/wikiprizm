---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Bdf = == Description == This is factor B\ndegrees\
    \ of freedom. It is used like a number. ANOVA test must be run\nin order to use\
    \ this. == Syntax ==\u2019\u2018\u2019Bdf\u2019\u2019\u2019 == Example == Bdf\
    \ \u2026\u2019"
  timestamp: '2012-02-15T23:43:56Z'
title: Bdf
---

# Bdf

## Description

This is factor B degrees of freedom. It is used like a number. ANOVA
test must be run in order to use this.

## Syntax

**Bdf**

## Example

`Bdf`
