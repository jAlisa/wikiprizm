---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Amt_INT( = == Description == This\ncommand returns\
    \ the interest paid for payment PM1. == Syntax\n==\u2019\u2018\u2019Amt_INT(\u2019\
    \u2019\u2019\u2018\u2019PM1\u2019\u2018,\u2019\u2018PM2\u2019\u2018,\u2019\u2018\
    I%\u2019\u2018,\u2019\u2018PV\u2019\u2018,\u2019\u2018PMT\u2019\u2018,\u2019\u2018\
    P/Y\u2019\u2018,\u2019\u2018C/Y\u2019\u2019\u2019\u2019\u2018)\u2019\u2019\u2019\
    \n==\u2026\u2019"
  timestamp: '2012-02-15T23:36:04Z'
title: Amt_INT(
---

# Amt_INT(

## Description

This command returns the interest paid for payment PM1.

## Syntax

**Amt_INT(***PM1*,*PM2*,*I%*,*PV*,*PMT*,*P/Y*,*C/Y***)**

## Example

`Amt_INT(12,10,5.5,1000,1000,12,12)`
