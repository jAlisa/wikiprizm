---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= E = == Description == This is used for\nscientific\
    \ notation.(You can think E as 10^) == Syntax\n==\u2019\u2018Value\u2019\u2019\
    \u2019\u2018\u2019E\u2019\u2019\u2019\u2018\u2019Value\u2019\u2019 == Example\
    \ == 2E4\n\\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-22T03:21:25Z'
title: E
---

# E

## Description

This is used for scientific notation.(You can think E as 10^)

## Syntax

*Value***E***Value*

## Example

`2E4`
