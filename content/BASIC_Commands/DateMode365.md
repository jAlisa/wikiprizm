---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= DateMode365 = == Description == This\ncommand\
    \ sets number of days in the year to 365 in financial\ncalculation. == Syntax\
    \ ==\u2019\u2018\u2019DateMode365\u2019\u2019\u2019 == Example == DateMode365\n\
    \\[\\[Cat\u2026\u2019"
  timestamp: '2012-02-15T12:07:50Z'
title: DateMode365
---

# DateMode365

## Description

This command sets number of days in the year to 365 in financial
calculation.

## Syntax

**DateMode365**

## Example

`DateMode365`
