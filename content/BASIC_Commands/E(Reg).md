---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= e(Reg) = == Description == This is\nthe\u2019\
    \u2018\u2019e\u2019\u2019\u2019 value used in regression. Can be treated as a\
    \ number.\nRegression including \u2019\u2018\u2019e\u2019\u2019\u2019 must be\
    \ calculated before usage. ==\nSyntax ==\u2026\u2019"
  timestamp: '2012-02-15T20:24:58Z'
title: E(Reg)
---

# e(Reg)

## Description

This is the **e** value used in regression. Can be treated as a number.
Regression including **e** must be calculated before usage.

## Syntax

**e**

## Example

`e`

`e+4`
