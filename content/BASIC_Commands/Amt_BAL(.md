---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Amt_BAL( = == Description == This\ncommand returns\
    \ the remaining principal balance following payment\nPM2. == Syntax\n==\u2019\u2018\
    \u2019Amt_BAL(\u2019\u2019\u2019\u2018\u2019PM1\u2019\u2018,\u2019\u2018PM2\u2019\
    \u2018,\u2019\u2018I%\u2019\u2018,\u2019\u2018PV\u2019\u2018,\u2019\u2018PMT\u2019\
    \u2018,\u2019\u2018P/Y\u2019\u2026\u2019"
  timestamp: '2012-02-15T23:35:23Z'
title: Amt_BAL(
---

# Amt_BAL(

## Description

This command returns the remaining principal balance following payment
PM2.

## Syntax

**Amt_BAL(***PM1*,*PM2*,*I%*,*PV*,*PMT*,*P/Y*,*C/Y***)**

## Example

`Amt_BAL(12,10,5.5,1000,1000,12,12)`
