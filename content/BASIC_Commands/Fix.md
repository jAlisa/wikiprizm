---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Fix = == Description == This command is\nused\
    \ to specify the number of digits displayed after decimal point.\n== Syntax ==\u2019\
    \u2018\u2019Fix\u2019\u2019\u2019 \u2018\u2019Number\u2019\u2019 == Example ==\
    \ Fix 3\u2019"
  timestamp: '2012-02-17T00:49:56Z'
title: Fix
---

# Fix

## Description

This command is used to specify the number of digits displayed after
decimal point.

## Syntax

**Fix** *Number*

## Example

`Fix 3`
