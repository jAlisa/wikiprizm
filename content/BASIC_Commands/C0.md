---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= c0 = == Description == This is the c0\nvalue\
    \ used in recursion. Can be used like the other variables. ==\nSyntax ==\u2019\
    \u2018\u2019c0\u2019\u2019\u2019 == Example == c0 ?\u2192c0 \\[\\[Category:BASIC_Comman\u2026\
    \u2019"
  timestamp: '2012-02-15T20:12:02Z'
title: C0
---

# c0

## Description

This is the c0 value used in recursion. Can be used like the other
variables.

## Syntax

**c0**

## Example

`c0`

`?→c0`
