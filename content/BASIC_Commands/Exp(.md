---
revisions:
- author: YeongJIN COOL
  comment: /\* Syntax \*/
  timestamp: '2012-02-15T03:35:25Z'
title: Exp(
---

# Exp(

## Description

This command converts specified string into expression.

## Syntax

**Exp(***"String"***)**

## Example

`Exp("3+5")`
