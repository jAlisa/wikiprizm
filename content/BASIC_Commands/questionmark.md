---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= ? = == Description == This command\ntakes the\
    \ input from user and stores it into specified variable. ==\nSyntax ==\u2019\u2019\
    \u2018?\u2019\u2019\u2019 \u2192 Variable == Example == ?\u2192X\n\\[\\[Category:BASIC_C\u2026\
    \u2019"
  timestamp: '2012-02-15T03:28:05Z'
title: questionmark
---

# ?

## Description

This command takes the input from user and stores it into specified
variable.

## Syntax

**?** → Variable

## Example

`?→X`
