---
revisions:
- author: Turiqwalrus
  comment: "Created page with \u2018== GridOff == == Description == This\ncommand\
    \ turns the graph screen\u2019s grid off. == Syntax == GridOff (no\narguments)\
    \ == Example == GridOff \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-29T20:13:48Z'
title: GridOff
---

## GridOff

## Description

This command turns the graph screen's grid off.

## Syntax

GridOff (no arguments)

## Example

`GridOff`
