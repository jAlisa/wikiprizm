---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= cos = == Description == This command\nsolves\
    \ the cosine of the value. == Syntax ==\u2019\u2018\u2019cos\u2019\u2019\u2019\
    \ \u2018\u2019value\u2019\u2019 ==\nExample == cos 30 \\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-15T03:32:52Z'
title: Cos
---

# cos

## Description

This command solves the cosine of the value.

## Syntax

**cos** *value*

## Example

`cos 30`
