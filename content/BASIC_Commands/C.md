---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= C = == Description == This is the\ncommand to\
    \ get a combination. == Syntax ==\u2019\u2018n\u2019\u2019\u2019\u2018\u2019C\u2019\
    \u2019\u2019\u2018\u2019r\u2019\u2019 ==\nExample == 3C2 \\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-15T23:46:22Z'
title: C
---

# C

## Description

This is the command to get a combination.

## Syntax

*n***C***r*

## Example

`3C2`
