---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cmpd_n( = == Description == This command\nreturns\
    \ number of compound periods. == Syntax\n==\u2019\u2018\u2019Cmpd_n(\u2019\u2019\
    \u2019\u2018\u2019I%\u2019\u2018,\u2019\u2018PV\u2019\u2018,\u2019\u2018PMT\u2019\
    \u2018,\u2019\u2018FV\u2019\u2018,\u2019\u2018P/Y\u2019\u2018,\u2019\u2018C/Y\u2019\
    \u2019\u2019\u2019\u2018)\u2019\u2019\u2019\n== Example == Cmpd\\_\u2026\u2019"
  timestamp: '2012-02-16T00:11:10Z'
title: Cmpd_n(
---

# Cmpd_n(

## Description

This command returns number of compound periods.

## Syntax

**Cmpd_n(***I%*,*PV*,*PMT*,*FV*,*P/Y*,*C/Y***)**

## Example

`Cmpd_n(5.5,1000,10,0,12,12)`
