---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cnvt_EFF( = == Description == This\ncommand\
    \ returns the interest rate converted from the nominal\ninterest rate to the effective\
    \ interest rate. == Syntax\n==\u2019\u2018\u2019Cnvt_EFF(\u2019\u2019\u2019\u2018\
    \u2019n\u2019\u2018\u2026\u2019"
  timestamp: '2012-02-16T00:20:18Z'
title: Cnvt_EFF(
---

# Cnvt_EFF(

## Description

This command returns the interest rate converted from the nominal
interest rate to the effective interest rate.

## Syntax

**Cnvt_EFF(***n*,*I%***)**

## Example

`Cnvt_EFF(10,5.5)`
