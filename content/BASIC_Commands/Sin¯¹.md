---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= sin\xAF\xB9 = == Description == This command\n\
    returns the arcsine of the value. == Syntax ==\u2019\u2018\u2019sin\xAF\xB9\u2019\
    \u2019\u2019 \u2018\u2019Value\u2019\u2019\n== Example == sin\xAF\xB9 .75\u2019"
  timestamp: '2012-02-29T20:22:03Z'
title: "Sin\xAF\xB9"
---

# sin¯¹

## Description

This command returns the arcsine of the value.

## Syntax

**sin¯¹** *Value*

## Example

`sin¯¹ .75`
