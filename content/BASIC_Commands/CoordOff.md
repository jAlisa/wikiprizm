---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-29T20:08:43Z'
title: CoordOff
---

# CoordOff

## Description

This command stops the coordinates from displaying in the graph screen.

## Syntax

CoordOff (no arguments)

## Example

`CoordOff`
