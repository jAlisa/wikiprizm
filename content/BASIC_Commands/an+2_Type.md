---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018=an+2 Type = == Description == This\ncommand sets\
    \ recursion type to an+2. == Syntax ==\u2019\u2018\u2019an+2 Type\u2019\u2019\u2019\
    \ ==\nExample == an+2 Type \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T23:40:36Z'
title: an+2_Type
---

# an+2 Type {#an2_type}

## Description

This command sets recursion type to an+2.

## Syntax

**an+2 Type**

## Example

`an+2 Type`
