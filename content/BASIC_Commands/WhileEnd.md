---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-24T20:27:06Z'
title: WhileEnd
---

# WhileEnd

## Description

This command is used to create While loop along with **While**.

## Syntax

**While** *Condition*

Code...

**WhileEnd**

## Example

    While X=3
    X+1→X
    WhileEnd
