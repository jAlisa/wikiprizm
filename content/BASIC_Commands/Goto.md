---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Goto = == Description == This command is\nused\
    \ to jump to specified label. == Syntax ==\u2019\u2018\u2019Goto\u2019\u2019\u2019\
    \n\u2018\u2019Label_Name\u2019\u2019 == Example == Goto A\u2019"
  timestamp: '2012-02-17T00:46:30Z'
title: Goto
---

# Goto

## Description

This command is used to jump to specified label.

## Syntax

**Goto** *Label_Name*

## Example

`Goto A`
