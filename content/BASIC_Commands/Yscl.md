---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Yscl = == Description == This command\nsets\
    \ the scale of y displayed in the graphscreen. == Syntax\n==\u2019\u2018Value\u2019\
    \u2018\u2192Yscl == Example == 3\u2192\u2019\u2018\u2019Yscl\u2019\u2019\u2019\
    \n\\[\\[Category:BASIC_Commands\u2026\u2019"
  timestamp: '2012-02-24T20:32:51Z'
title: Yscl
---

# Yscl

## Description

This command sets the scale of y displayed in the graphscreen.

## Syntax

*Value*→Yscl

## Example

`3→`**`Yscl`**
