---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-29T20:02:18Z'
title: Min(
---

# Min(

## Description

This command returns the manimum value from the given numbers.

## Syntax

**Min(***List***)**

## Example

`Min(List1`
