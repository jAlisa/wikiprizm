---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= \u201D = == Description == This command\nspecifies\
    \ the strings. == Syntax ==\u2019\u2019\u2018\u201C\u2019\u2019\u2019\u2018\u2019\
    String\u2019\u2019\u2019\u2019\u2018\u201C\u2019\u2019\u2019 ==\nExample ==\u201D\
    HELLO\u201D \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-24T20:40:37Z'
title: quote
---

# "

## Description

This command specifies the strings.

## Syntax

**"***String***"**

## Example

`"HELLO"`
