---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= cos\u02C9\xB9 = == Description == This command\n\
    returns the inverse cosine of the value. == Syntax ==\u2019\u2018\u2019cos\u02C9\
    \xB9\u2019\u2019\u2019\n\u2018\u2019value\u2019\u2019 == Example == cos\u02C9\xB9\
    \ 30 \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T12:11:47Z'
title: "Cos\u02C9\xB9"
---

# cosˉ¹

## Description

This command returns the inverse cosine of the value.

## Syntax

**cosˉ¹** *value*

## Example

`cosˉ¹ 30`
