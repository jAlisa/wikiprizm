---
title: PxlChg
---

## PxlChg

## Description

This command inverts a pixel on the graph screen(if the pixel was on, it
gets turned off, and vice versa).

## Syntax

`PxlChg <X-coordinate>,<Y-coordinate>`

## Example

`PxlChg 10,10`
