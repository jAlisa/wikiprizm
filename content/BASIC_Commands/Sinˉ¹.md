---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Sin\u02C9\xB9 = == Description == This command\n\
    returns the inverse sine of the value. == Syntax ==\u2019\u2018\u2019sin\u02C9\
    \xB9\u2019\u2019\u2019\n\u2018\u2019value\u2019\u2019 == Example == sin\u02C9\xB9\
    \ 30 \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T12:13:15Z'
title: "Sin\u02C9\xB9"
---

# Sinˉ¹

## Description

This command returns the inverse sine of the value.

## Syntax

**sinˉ¹** *value*

## Example

`sinˉ¹ 30`
