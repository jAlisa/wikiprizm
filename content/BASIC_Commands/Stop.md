---
revisions:
- author: Turiqwalrus
  comment: "Created page with \u2018== Stop == == Description == This command\nstops\
    \ the program immediately == Syntax == Stop (no arguments) ==\nExample == Stop\
    \ \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-29T20:30:53Z'
title: Stop
---

## Stop

## Description

This command stops the program immediately

## Syntax

Stop (no arguments)

## Example

`Stop`
