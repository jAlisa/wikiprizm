---
title: Mean(
---

## Description

Calculates the [Arithmetic
mean](http://en.wikipedia.org/wiki/Arithmetic_mean) of a number.

## Syntax

Mean(\<List name>)

## Example

Stores data to list 'List 1' and then displays
the mean of that list at (1,1).

```
{1,5,2,7,3} → List 1
Locate 1,1,Mean(List 1)
```