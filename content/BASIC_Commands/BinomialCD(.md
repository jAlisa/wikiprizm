---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= BinomialCD( = == Description == This\ncommand\
    \ solves the cumulative binomial distribution with probability\nP, total number\
    \ n, and event\u2019\u2018x\u2019\u2018. == Syntax\n==\u2019\u2018\u2019BinomialCD(\u2019\
    \u2019\u2018\\[\u2019\u2018\u2026\u2019"
  timestamp: '2012-02-15T03:20:22Z'
title: BinomialCD(
---

# BinomialCD(

## Description

This command solves the cumulative binomial distribution with
probability P, total number n, and event *x*.

## Syntax

**BinomialCD(**\[*x*,\]n,P)

x is an optional value and can be either single value or list.

## Example

`BinomialCD(3,5,.6)`
