---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Next = == Description == This command\ncreates\
    \ counter loop along with\u2019\u2018\u2019To\u2019\u2019\u2019 and \u2019\u2018\
    \u2019For\u2019\u2019\u2019 == Syntax ==\n\u2019\u2018\u2019For\u2019\u2019\u2019\
    \ \u2018\u2019Start_Num\u2019\u2019 \u2192 \u2018\u2019Variable\u2019\u2019 \u2019\
    \u2018\u2019To\u2019\u2019\u2019 \u2018\u2019End_Num\u2019\u2019 \u2026Stu\u2026\
    \u2019"
  timestamp: '2012-02-15T20:10:26Z'
title: Next
---

# Next

## Description

This command creates counter loop along with **To** and **For**

## Syntax

**For** *Start_Num* → *Variable* **To** *End_Num*

...Stuffs...

**Next**

## Example

    For 1→A To 21
    Blue Locate A,1,"X"
    Next
