---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= 3\u221A = == Description == This command\nreturns\
    \ the cube root of the value. == Syntax ==\u2019\u2018\u20193\u221A\u2019\u2019\
    \u2019\u2018\u2019Value\u2019\u2019 ==\nExample == 3\u221A27 \\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-24T20:14:20Z'
title: "3\u221A"
---

# 3√

## Description

This command returns the cube root of the value.

## Syntax

**3√***Value*

## Example

`3√27`
