---
revisions:
- author: KermMartian
  comment: /\* Example \*/
  timestamp: '2014-07-03T20:22:20Z'
title: Dim
---

## Description

Finds the number of elements in an inputted list

## Syntax

`Dim <List name>`

## Example

    {1,4,2,5,2,3} → List 1              //Stores data into 'List 1', and then displays its length at (1,1)
    Locate 1,1,Dim List 1
