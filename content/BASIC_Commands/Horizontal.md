---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Horizontal = == Description == This\ncommand\
    \ draws the horizontal line y=given number at graphscreen. ==\nSyntax ==\u2019\
    \u2018\u2019Horizontal\u2019\u2019\u2019 \u2018\u2019Value\u2019\u2019 == Example\
    \ == Horizontal 0\n\\[\\[Ca\u2026\u2019"
  timestamp: '2012-02-24T20:24:07Z'
title: Horizontal
---

# Horizontal

## Description

This command draws the horizontal line y=given number at graphscreen.

## Syntax

**Horizontal** *Value*

## Example

`Horizontal 0`
