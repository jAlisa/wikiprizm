---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= I% = == Description == This is the\nannual interest\
    \ value used in financial calculation. This can be\ntreated as a variable. ==\
    \ Syntax ==\u2019\u2018\u2019I%\u2019\u2019\u2019 == Example == 5.5\u2192I%\n\
    3\\*I% \u2026\u2019"
  timestamp: '2012-02-16T00:14:23Z'
title: I%
---

# I%

## Description

This is the annual interest value used in financial calculation. This
can be treated as a variable.

## Syntax

**I%**

## Example

    5.5→I%
    3*I%
