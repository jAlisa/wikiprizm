---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= PMT = == Description == This is the\npayment\
    \ amount in financial calculation. Can be used as a variable.\n== Syntax ==\u2019\
    \u2018\u2019PMT\u2019\u2019\u2019 == Example == PMT 100\u2192PMT\n\\[\\[Category:BASIC\\\
    _\u2026\u2019"
  timestamp: '2012-02-16T23:33:24Z'
title: PMT
---

# PMT

## Description

This is the payment amount in financial calculation. Can be used as a
variable.

## Syntax

**PMT**

## Example

    PMT
    100→PMT
