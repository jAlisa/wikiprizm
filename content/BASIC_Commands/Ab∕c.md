---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-15T20:02:40Z'
title: "Ab\u2215c"
---

# Ab/c

## Description

This command sets the calculator to display answer as Ab/c if possible.

## Syntax

**Ab/c**

## Example

`Ab/c`
