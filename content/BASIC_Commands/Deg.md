---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Deg = == Description == This command\nsets calculator\
    \ to Degree mode. == Syntax ==\u2019\u2018\u2019Deg\u2019\u2019\u2019 == Example\
    \ ==\nDeg \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T03:34:22Z'
title: Deg
---

# Deg

## Description

This command sets calculator to Degree mode.

## Syntax

**Deg**

## Example

`Deg`
