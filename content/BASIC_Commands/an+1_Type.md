---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018=an+1 Type = == Description == This\ncommand sets\
    \ recursion type to an+1. == Syntax ==\u2019\u2018\u2019an+1 Type\u2019\u2019\u2019\
    \ ==\nExample == an+1 Type \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-15T23:40:15Z'
title: an+1_Type
---

# an+1 Type {#an1_type}

## Description

This command sets recursion type to an+1.

## Syntax

**an+1 Type**

## Example

`an+1 Type`
