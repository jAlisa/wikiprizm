---
revisions:
- author: ProgrammerNerd
  comment: "Don\u2019t use a hard-coded address."
  timestamp: '2021-05-01T23:14:02Z'
title: FillArea
---

## Synopsis

Fills a rectangular area of (width,height) with upper-left corner at
(x,y)

## Source code {#source_code}

    void fillArea(unsigned x,unsigned y,unsigned w,unsigned h,unsigned short col){
        unsigned short*s=(unsigned short*)GetVRAMAddress();
        s+=(y*384)+x;
        while(h--){
            unsigned w2=w;
            while(w2--)
                *s++=col;
            s+=384-w;
        }
    }

## Inputs

x - X coordinate of the upper-left corner.\
x - Y coordinate of the upper-left corner.\
w - Width of the filled rectangle.\
h - Height of the filled rectangle.\
col - Color of the filled rectangle.\

## Comment

Function written by ProgrammerNerd
