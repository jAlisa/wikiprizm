---
revisions:
- author: Flyingfish
  timestamp: '2013-03-19T01:44:19Z'
title: Floor
---

## Synopsis

Round a number downwards to the nearest integer.

### Definition

    float floor(x) {
        return ((int)x) - (x < 0);
    }

### Inputs

**x** - number to be rounded down.

### Outputs

Returns rounded number.

## Credits

Tari
