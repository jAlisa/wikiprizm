---
revisions:
- author: Dr-carlos
  comment: Add note about PGRM_GetKey being included in libfxcg.
  timestamp: '2021-10-12T09:36:48Z'
title: PRGM_GetKey
---

## Synopsis

This is a routine that is non-blocking\* <sup>*See\ Comments*</sup>,
goes through the OS, and returns one key value: the very last one
pressed or still held.

### Definition

    int PRGM_GetKey(void)
    {
      unsigned char buffer[12];
      PRGM_GetKey_OS( buffer );
      return ( buffer[1] & 0x0F ) * 10 + ( ( buffer[2] & 0xF0 ) >> 4 );
    }

### Inputs

None

### Outputs

Packed key code

## Comments

This function will retrieve the last pressed key and continue returning
it if it is still held. While it's slightly outdated, it is still
commonly used for simple key checking, and is included in libfxcg in the
header \<fxcg/keyboard.h\>.

The return is a packed 2 digit number, with the first digit being the
*row - 1* and the second digit being the *column*. For example,
GetKeyWait_OS, when F1 is pressed, will give 7,10 as the col,row. This
function returns 79 as the (col)(row-1). AC/ON returns 1,1 for
GetKeyWait_OS and therefore returns 10 from PRGM_GetKey.

\* This function actually **does** block for a very small amount of
time, which can hurt performance of FPS-hungry add-ins. The
PRGM_GetKey_OS syscall will stall for a small amount of time if no keys
are pressed, and unnoticeable delay when a key is pressed. If you are
making a time-sensative add-in, it is best to not use syscalls and
[directly poll the keyboard
hardware]({{< ref "Useful_Routines/Debouncing_Multiple-Keypress_PRGM_GetKey.md" >}}).

## Keycodes

    [LEFT]  38     [F1]    79     [SHIFT] 78     [ALPHA] 77
    [DOWN]  37     [F2]    69     [OPTN]  68     [x^2]   67
    [UP]    28     [F3]    59     [VARS]  58     [^]     57
    [RIGHT] 27     [F4]    49     [MENU]  48     [EXIT]  47
                   [F5]    39
                   [F6]    29
     
    [X/O/T] 76     [a b/c] 75
    [log]   66     [F<->D] 65 
    [ln]    56     [(]     55 
    [sin]   46     [)]     45
    [cos]   36     [,]     35 
    [tan]   26     [->]    25
     
    [7]     74     [4]     73     [1]     72     [0]     71
    [8]     64     [5]     63     [2]     62     [.]     61
    [9]     54     [6]     53     [3]     52     [EXP]   51
    [DEL]   44     [*]     43     [+]     42     [(-)]   41
    [AC/ON] 10     [/]     33     [-]     32     [EXE]   31
