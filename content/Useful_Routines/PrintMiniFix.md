---
revisions:
- author: KermMartian
  comment: /\* Comments \*/
  timestamp: '2012-09-06T17:18:30Z'
title: PrintMiniFix
---

## Synopsis

This is a routine that displays a text in small (glyph) font on the
screen.

### Definition

    static const short empty[18] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    int PrintMiniFix(int x, int y, const char *Msg, const int flags, const short color, const short bcolor){
        int i = 0, dx;
        unsigned short width;
        void *p;

       while ( Msg[i] ){
          p = GetMiniGlyphPtr( Msg[i], &width );
          dx = ( 12 - width ) / 2;
          if ( dx > 0 ) {
             PrintMiniGlyph( x, y, (void*)empty, flags, dx, 0, 0, 0, 0, color, bcolor, 0 );
          }else dx = 0;
          PrintMiniGlyph( x+dx, y, p, flags, width, 0, 0, 0, 0, color, bcolor, 0 );
          if ( width+dx < 12 ){
             PrintMiniGlyph( x+width+dx, y, (void*)empty, flags, 12-width-dx, 0, 0, 0, 0, color, bcolor, 0 );
          }
          x += 12;
          i++;
       }
       return x;
    }

### Inputs

-   *int* **x**: X-coordinate of top-left of text
-   *int* **y**: Y-coordinate of top-left of text
-   *const char\** **Msg**: string to display
-   *const int* **flags**:

| Symbol                     | Value      |
|----------------------------|------------|
| PMF_ALLOW_STATUSAREA       | 0x40       |
| PMF_INVERT                 | 4          |
| PMF_DO_NOT_USE_BACKCOLOR   | 2          |
| PMF_UPPER_X\_LIMIT_IGNORED | 0xFFFFFFFF |

-   *const short* **color**: Text foreground color
-   *const short* **bcolor**: Text background color

### Outputs

Small font string on-screen.

### Comments

-   Flags come from an old version of disp_tools.hpp. No further
    information on them is known.
-   Without PMF_ALLOW_STATUSAREA, the syscall *will* try to draw the
    text even with a negative y value, but it will clip as though the
    top bar was shown, regardless of whether that top bar is actually
    present or not.

### Usage Example {#usage_example}

    PrintMiniFix(0, 175,
             "Oh look a string at the bottom of the screen.",
             0, COLOR_WHITE, COLOR_BLACK);
