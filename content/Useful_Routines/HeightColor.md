---
revisions:
- author: Ashbad
  comment: /\* Synopsis \*/
  timestamp: '2012-05-02T20:00:30Z'
title: HeightColor
---

## Synopsis

This routine returns a short unsigned int (aka a color_t) corresponding
to a color between red and blue.

### Definition

    short unsigned int heightcolor(float z, float z_min, float z_max) {
             float frac = ((z-z_min)/(z_max-z_min));
             
             //color!
             float r = (0.25f)-frac;
             float g = (0.5f)-frac;
             float b = (0.75f)-frac;

             //calculate the R/G/B values
             r = (r>0.f)?r:-r; g = (g>0.f)?g:-g; b = (b>0.f)?b:-b;   //absolute value
             r = (0.25f)-r; g = (1.f/3.f)-g; b = (0.25f)-b;   //invert
             r = (r>0.f)?(6.f*r):0.f; g = (g>0.f)?(6.f*g):0.f; b = (b>0.f)?(6.f*b):0.f;   //scale the chromatic triangles
             r = (r>1.f)?1.f:r; g = (g>1.f)?1.f:g; b = (b>1.f)?1.f:b;   //clip the top of the chromatic triangles
             if (frac < 0.25f) r = (r+1.f)/2.f;   //adjust the bottom end of the scale so that z_min is red, not black
             if (frac > 0.75f) b = (b+1.f)/2.f;   //adjust the top end of the scale so that z_max is blue, not black
             return (short unsigned int)(0x0000ffff & (((int)(31.f*r) << 11) | ((int)(63.f*g) << 5) | ((int)(31.f*b))));   //put the bits together
    }

### Inputs

-   *float* **z**: The current z value from which to get a color
-   *float* **z_min**: The minimum value for any z
-   *float* **z_max**: The maximum value for any z

### Outputs

A color between red and blue (with green in the middle)

### Notes

Used in Graph3DP to determine the color of points in a 3D wireframe
graph. Values near the minimum value are more red, and ones nearer to
the top are blue. Very useful for color representations of a range or
for 3D depth buffering.

## Comments

Created by Christopher "Kerm Martian" Mitchell.
