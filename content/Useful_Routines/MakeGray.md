---
revisions:
- author: ProgrammerNerd
  comment: /\* Synopsis \*/
  timestamp: '2014-04-18T01:34:43Z'
title: MakeGray
---

## Synopsis

Creates a two-byte 5/6/5 color code based on a grey value.

## Source code {#source_code}

    int makeGray(int shade) {
       return shade * 0x0841;
    }

## Inputs

shade which can be between 0 (black) and 31 (white).

## Comments

Function originally written by calc84maniac.
