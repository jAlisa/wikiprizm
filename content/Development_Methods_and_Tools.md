---
revisions:
- author: Tari
  comment: Remove obsolete SDK pointers
  timestamp: '2020-04-11T06:52:38Z'
title: Development_Methods_and_Tools
---

This page contains information on a number of ways to program the Prizm.

## Casio BASIC {#casio_basic}

This is the method with the lowest barrier to entry; it's also the only
one supported by Casio. It consists of making programs that run within
the "Program" menu of the calculator, in a BASIC dialect that is common
to many Casio calculators. An essential guide on how to program in Casio
BASIC on the Prizm is detailed in Chapter 8 of the [fx-CG 10 / 20
software
manual](http://support.casio.com/storage/en/manual/pdf/EN/004/fx-CG10_20_Soft_EN.pdf).
BASIC programs are most useful for simple programs that work with
formulas, for example. Being interpreted with a slow interpreter,
programs are not especially fast and are quite limited in terms of what
they can do. They can't be very big, either, since they are stored in
the Main Memory which has only 61 KB of space available for the user.
Their only advantages are the ability to directly use the catalog of
math functions and graphing tools, and the fact that they can be created
and edited on the calculator, without a personal computer or additional
tools.

## LuaZM

LuaZM is a port of the core features of Lua 5.2.1 to the Casio Prizm.
Available [since September
2012](http://www.cemetech.net/forum/viewtopic.php?t=8168), its latest
release happened later in the same month, and development has since then
paused for an undetermined amount of time. LuaZM scripts allow for finer
graphics than BASIC programs, and are faster too. However, the lack of
support for many Lua libraries and some bugs can deem it inappropriate
for more complex programs. LuaZM includes a REPL like the desktop
command-line version, however, programs can not be edited directly on
the calculator (except through the use of a text editor add-in). Lua
scripts can not make use of the function catalog available to BASIC
programs, either. A possibly outdated reference on LuaZM-specific
functions can be found [here]({{< ref "LuaZM_Reference.md" >}}).

## Add-in Development {#add_in_development}

Add-in software appears as additional icons on the calculator's Main
Menu, and in some cases as additional eActivity strips. Add-ins are
compiled and run directly on the calculator's CPU, having privileged
access to the whole system. Because of this, add-ins allow for the most
powerful 3rd-party programs. The calculator enthusiast community has
known how to make custom add-ins for the Prizm since at least April
2011. The downside to add-in development is that it's much more complex
than the previously mentioned methods, definitely can't be done
on-calculator, and since they are so powerful, they can even destroy the
calculator. You have been warned, but don't fear: if you stick to a
number of well-tested features and don't do anything too *exotic*, it's
unlikely that you'll cause any problem.

A number of Software Development Kits (SDK) have been made to facilitate
add-in development, but unlike what happened with the fx-9860G, Casio
has not released an official one. At Cemetech, the most used SDK is
PrizmSDK, which includes as core component a library called
[libfxcg](https://github.com/Jonimoose/libfxcg/). This SDK compiles
add-ins using [GCC](https://gcc.gnu.org/), cross-compiled to target SH4A
CPUs. This is unlike other SDKs which use Renesas's proprietary
compilers along with a mix of libraries and tools from the fx-9860G
official SDK modified to work with the Prizm. Another core component is
[mkg3a](https://www.taricorp.net/projects/mkg3a), a tool that takes the
result of the compilation and packages it in [G3A
format]({{< ref "G3A_File_Format.md" >}}), ready for use on the
calculator.

Unless otherwise noted, information in this Wiki, namely the syscall
names, is relative to the PrizmSDK and libfxcg.

The [SDK setup guide]({{< ref "Tutorials/PrizmSDK_Setup_Guide.md" >}})
discusses how to get started building add-ins.
