#!/usr/bin/env python
import itertools
from pandocfilters import toJSONFilters, Link, Para, CodeBlock

def fix_ref_underscores(key, value, format, meta):
    if key == 'Link':
        target = value[2][0]
        if target.startswith('{{<') and target.endswith('>}}'):
            head, _, mid = target.partition('"')
            mid, _, tail = mid.rpartition('"')
            value[2][0] = '"'.join((
                head.replace('_', ' '),
                mid,
                tail.replace('_', ' '),
            ))
            return Link(*value)

def remove_wikilink_class(key, value, format, meta):
    if key == 'Link':
        if value[2][1] == 'wikilink':
            value[2][1] = ''
        return Link(*value)

def consolidate_code_lines(key, value, format, meta):
    if key == 'Para':
        split_para = []
        curr_inlines = []

        def emit_inlines():
            if len(curr_inlines) > 1:
                # Block ends; replace with an actual code block made up of
                # each line's text concatenated together, ignoring the line
                # breaks. Also replace NBSPs with regular spaces.
                text = '\n'.join(
                    c['c'][1].replace('\u00a0', ' ')
                    for c in curr_inlines if c['t'] == 'Code'
                )
                split_para.append(CodeBlock(['', [], []], text))
            else:
                split_para.extend(curr_inlines)
            curr_inlines.clear()

        for element in value:
            if element['t'] in ('Code', 'LineBreak'):
                curr_inlines.append(element)
            else:
                emit_inlines()
                split_para.append(element)
        emit_inlines()

        # We had a paragraph input, which is a block. Since we're emitting code
        # blocks, we also need to wrap groups of inlines in paragraphs and emit
        # a collection of blocks. A paragraph containing code blocks is not
        # valid.
        out = []
        for _, chunk in itertools.groupby(split_para, key=lambda e: e['t'] == 'CodeBlock'):
            chunk = list(chunk)
            if chunk[0]['t'] == 'CodeBlock':
                assert all(e['t'] == 'CodeBlock' for e in chunk)
                out.extend(chunk)
            else:
                out.append(Para(chunk))
        return out

if __name__ == '__main__':
    toJSONFilters([remove_wikilink_class, fix_ref_underscores,
                   consolidate_code_lines])
